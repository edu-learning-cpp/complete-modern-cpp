//
// Created by Eduard Luhtonen on 16/08/2020.
//

#include "Checking.h"
#include <iostream>

Checking::Checking(const string &name, float amount, float minimumBalance) : Account(name, amount),
                                                                             minimumBalance(minimumBalance) {
}

Checking::~Checking() {
}

void Checking::withdraw(float amount) {
    if ((balance - amount) > minimumBalance) {
        Account::withdraw(amount);
    } else {
        std::cout << "Invalid amount" << std::endl;
    }
}

float Checking::geMinimumBalance() const {
    return minimumBalance;
}
