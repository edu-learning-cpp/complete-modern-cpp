//
// Created by Eduard Luhtonen on 16/08/2020.
//

#ifndef INC_02_ACCOUNT_SOLUTION_SAVINGS_H
#define INC_02_ACCOUNT_SOLUTION_SAVINGS_H

#include "Account.h"

class Savings : public Account {
    float rate;
public:
    Savings(const string &name, float amount, float rate);
    ~Savings();

    float getInterestRate() override;
    void accumulateInterest() override;
};


#endif //INC_02_ACCOUNT_SOLUTION_SAVINGS_H
