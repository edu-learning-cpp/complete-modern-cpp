//
// Created by Eduard Luhtonen on 16/08/2020.
//

#ifndef INC_02_ACCOUNT_SOLUTION_CHECKING_H
#define INC_02_ACCOUNT_SOLUTION_CHECKING_H


#include "Account.h"

class Checking : public Account {
    float minimumBalance;
public:
    using Account::Account;
    Checking(const string &name, float amount, float minimumBalance);
    ~Checking();
    void withdraw(float amount) override;
    float geMinimumBalance() const;
};


#endif //INC_02_ACCOUNT_SOLUTION_CHECKING_H
