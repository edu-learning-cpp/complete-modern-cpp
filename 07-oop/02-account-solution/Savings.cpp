//
// Created by Eduard Luhtonen on 16/08/2020.
//

#include "Savings.h"

Savings::Savings(const string &name, float amount, float rate) : Account(name, amount), rate(rate) {
}

Savings::~Savings() {
}

float Savings::getInterestRate() {
    return rate;
}

void Savings::accumulateInterest() {
    balance += (balance * rate);
}
