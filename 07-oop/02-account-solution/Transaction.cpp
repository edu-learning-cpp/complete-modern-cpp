//
// Created by Eduard Luhtonen on 16/08/2020.
//

#include "Transaction.h"
#include <iostream>

void transact(Account *pAccount) {
    std::cout << "Transaction started" << std::endl;
    std::cout << "Initial balance: " << pAccount->getBalance() << std::endl;
    pAccount->deposit(100);
    pAccount->accumulateInterest();
    pAccount->withdraw(170);
    std::cout << "Interest rate: " << pAccount->getInterestRate() << std::endl;
    std::cout << "Final balance: " << pAccount->getBalance() << std::endl;
}
