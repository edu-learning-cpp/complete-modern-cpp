//
// Created by Eduard Luhtonen on 16/08/2020.
//

#include "Account.h"

int Account::ANGenerator = 1000;

Account::Account(const string &name, float amount) : name(name), balance(amount) {
    accNo = ++ANGenerator;
}

Account::~Account() {
}

const string &Account::getName() const {
    return name;
}

float Account::getBalance() const {
    return balance;
}

int Account::getAccNo() const {
    return accNo;
}

void Account::accumulateInterest() {

}

void Account::withdraw(float amount) {
    if (amount < balance) {
        balance -= amount;
    } else {
        cout << "Insufficient balance" << endl;
    }
}

void Account::deposit(float amount) {
    balance += amount;
}

float Account::getInterestRate() {
    return 0.0f;
}
