//
// Created by Eduard Luhtonen on 16/08/2020.
//

#ifndef INC_02_ACCOUNT_SOLUTION_ACCOUNT_H
#define INC_02_ACCOUNT_SOLUTION_ACCOUNT_H
#include <iostream>
using namespace std;

class Account {
    string name;
    int accNo;
    static int ANGenerator;
protected:
    float balance;
public:
    Account(const string &name, float amount);
    virtual ~Account();
    const string &getName() const;
    float getBalance() const;
    int getAccNo() const;

    virtual void accumulateInterest();
    virtual void withdraw(float amount);
    void deposit(float amount);
    virtual float getInterestRate();
};


#endif //INC_02_ACCOUNT_SOLUTION_ACCOUNT_H
