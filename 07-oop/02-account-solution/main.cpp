#include "Account.h"
#include "Savings.h"
#include "Checking.h"
#include "Transaction.h"
#include <iostream>
#include <typeinfo>

using namespace std;

int main() {
    Account acc("Bob", 1000);
    cout << "Initial balance: " << acc.getBalance() << endl;
    acc.deposit(200);
    acc.withdraw(380);
    cout << "Balance: " << acc.getBalance() << endl;
    cout << endl;

    Savings acc1("Bob", 1000, 0.5f);
    cout << "Initial balance: " << acc1.getBalance() << endl;
    acc1.deposit(200);
    acc1.accumulateInterest();
    acc1.withdraw(380);
    cout << "Balance: " << acc1.getBalance() << endl;
    cout << endl;

    Checking acc2("Bob", 1000, 50);
    cout << "Initial balance: " << acc2.getBalance() << endl;
    acc2.withdraw(980);
    cout << "Balance: " << acc2.getBalance() << endl;
    cout << endl;

    Checking acc3("Bob", 100, 50);
    transact(&acc3);
    cout << endl;

    Savings acc4("Bob", 100, 0.05f);
    transact(&acc4);
    cout << endl;

    // upcast and downcast
    Account *pAccount = &acc3; // upcast
    Checking *pChecking = static_cast<Checking *>(pAccount); // downcast

    cout << pChecking->geMinimumBalance() << endl;
    const type_info &ti1 = typeid(pAccount);
    cout << ti1.name() << endl;
    const type_info &ti2 = typeid(*pAccount);
    cout << ti2.name() << endl;
    if (ti2 == typeid(Checking)) {
        cout << "Pointing to Checking object" << endl;
    } else {
        cout << "Not pointing to Checking object" << endl;
    }
    cout << endl;

    pAccount = &acc4;
    pChecking = dynamic_cast<Checking *>(pAccount);
    if (pChecking != nullptr) {
        cout << "minimum balance: " << pChecking->geMinimumBalance() << endl;
    } else {
        cout << "Not a checking object"  << endl;
    }
    pAccount = &acc3;
    pChecking = dynamic_cast<Checking *>(pAccount);
    if (pChecking != nullptr) {
        cout << "minimum balance: " << pChecking->geMinimumBalance() << endl;
    } else {
        cout << "Not a checking object"  << endl;
    }
    cout << endl;

    return 0;
}
