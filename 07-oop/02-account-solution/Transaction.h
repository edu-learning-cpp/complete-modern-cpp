//
// Created by Eduard Luhtonen on 16/08/2020.
//

#ifndef INC_02_ACCOUNT_SOLUTION_TRANSACTION_H
#define INC_02_ACCOUNT_SOLUTION_TRANSACTION_H

#include "Account.h"

void transact(Account *pAccount);
#endif //INC_02_ACCOUNT_SOLUTION_TRANSACTION_H
