//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "two_d_arrays.h"

using std::cout, std::endl;

void two_d_arrays() {
    int data[2][3] = {
            1, 2, 3,
            4, 5, 6
    };
    cout << data[1][1] << endl;

    int *p1 = new int[3]{1, 2, 3};
    int *p2 = new int[3]{4, 5, 6};
    int **pData = new int *[2];
    pData[0] = p1;
    pData[1] = p2;
    cout << pData[1][1] << endl;

    delete[] p1;
    delete[] p2;
    delete[] pData;
    cout << endl;
}
