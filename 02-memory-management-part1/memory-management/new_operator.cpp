//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "new_operator.h"
using std::cout, std::endl;

void new_operator() {
    int *p = new int;
    *p = 5;
    cout << *p << endl;
    delete p;
    p = nullptr;

    int *p1 = new int(6);
    cout << *p1 << endl;
    delete p1;
    p1 = nullptr;

    cout << endl;
}
