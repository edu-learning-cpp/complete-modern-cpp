#include "using_malloc.h"
#include "new_operator.h"
#include "new_array_operator.h"
#include "two_d_arrays.h"

int main() {
    // Dynamic Memory Allocation - Part I (malloc function)
    using_malloc();
    // Dynamic memory allocation - part II (new operator)
    new_operator();
    // Dynamic memory allocation - part III (new[] operator)
    new_array_operator();
    // Dynamic memory allocation - part IV (2D arrays)
    two_d_arrays();
    return 0;
}
