//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "new_array_operator.h"

using std::cout, std::endl;

void strings() {
    char *p = new char[4];
    strcpy(p, "C++");
    cout << p << endl;
    delete[] p;
}

void new_array_operator() {
    int *p = new int[5];
    for (int i = 0; i < 5; i++) {
        p[i] = i;
    }
    for (int i = 0; i < 5; i++) {
        cout << p[i] << " ";
    }
    cout << endl;
    delete[] p;
    p = nullptr;

    int *p1 = new int[5]{1, 2, 3, 4, 5};
    for (int i = 0; i < 5; i++) {
        cout << p1[i] << " ";
    }
    cout << endl;
    delete[] p1;

    strings();
    cout << endl;
}
