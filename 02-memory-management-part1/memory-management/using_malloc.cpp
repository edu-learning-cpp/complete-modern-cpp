//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "using_malloc.h"
using std::cout, std::endl;

void using_malloc() {
    int *p = (int*)malloc(sizeof(int));
    *p = 5;
    printf("%d", *p);
    cout << endl;
    free(p);
    p = NULL;
    free(p);

    int *p1 = (int*)calloc(1, sizeof(int));
    if (p1 == NULL) {
        printf("failed to allocate memory\n");
        return;
    }
    *p1 = 6;
    printf("%d", *p1);
    cout << endl;
    free(p1);
    p1 = NULL;

    int *p2 = (int*)malloc(5 * sizeof(int)); // allocate memory for 5 integers
    if (p2 == NULL) {
        printf("failed to allocate memory\n");
        return;
    }
    *p2 = 7;
    printf("%d", *p2);
    cout << endl;
    free(p2);
    p2 = NULL;

    cout << endl;
}
