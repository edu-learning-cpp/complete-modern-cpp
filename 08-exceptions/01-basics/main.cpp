#include <iostream>
#include <memory>
#include <random>
#include <vector>
using namespace std;

class Test {
public:
    Test() {
        cout << "Test(): acquire resources" << endl;
    }
    ~Test() {
        cout << "~Test(): release resources" << endl;
    }
};

void processRecords(int count) {
    Test t1; // will be automatically deleted
    Test *t2 = new Test; // this should be deleted automatically
    unique_ptr<Test> t3(new Test); // smart pointer helps to bypass problem on previous line
    int *p = new int[count];
    if (count < 10) {
        throw out_of_range("Count should be greater than 10");
    }
    int *pArray = (int*)malloc(count * sizeof(int));
    if (pArray == nullptr) {
        throw runtime_error("Failed to allocate memory");
    }
    for (int i = 0; i < count; ++i) {
        pArray[i] = i;
    }
    free(pArray);
    delete t2; // on exception this will never execute
    delete [] p; // on exception will not be executed
}

void processRecords2(int count) {
    vector<int> p2;
    p2.reserve(count);
    if (count < 10) {
        throw out_of_range("Count should be greater than 10");
    }
    vector<int> records;
    for (int i = 0; i < count; ++i) {
        records.emplace_back(i);
    }

    default_random_engine eng;
    bernoulli_distribution dist;
    int errors{};
    for (int i = 0; i < count; ++i) {
        try {
            cout << "Processing record: " << records[i] << " ";
            if (!dist(eng)) {
                ++errors;
                throw runtime_error("Could not process the record");
            }
            cout << endl;
        } catch (runtime_error &ex) {
            cout << "[ERROR " << ex.what() << "]" << endl;
            if (errors > 4) {
                runtime_error err("Too many errors. Abandoning operation");
                ex = err;
                throw;
            }
        }
    }
}

class A {
public:
    A() {
        cout << "A()" << endl;
    }
    ~A() {
        cout << "~A()" << endl;
    }
};
class B {
public:
    B() {
        cout << "B()" << endl;
    }
    ~B() {
        cout << "~B()" << endl;
    }
};
class Test2 {
    A *pA{};
    B b{};
    int *pInt{};
    char *pStr{};
    int *pArr{};
public:
    Test2() {
        cout << "Test2(): acquire resources" << endl;
        pA = new A;
        pInt = new int;
        throw runtime_error("failed to initialise");
        pStr = new char[1000];
        pArr = new int[5000];
    }
    ~Test2() {
        cout << "~Test2(): release resources" << endl;
        delete pA;
        delete pInt;
        delete pStr;
        delete pArr;
    }
};
// solving problems of class T2
class Test3 {
    unique_ptr<A> pA{}; // use smart pointer instead of raw pointer
    B b{};
    unique_ptr<int> pInt{}; // use smart pointer instead of raw pointer
    string pStr{}; // use string instead of char array
    vector<int> pArr{}; // use vector instead of raw array
public:
    Test3() {
        cout << "Test3(): acquire resources" << endl;
        pA = std::make_unique<A>();
        throw runtime_error("failed to initialise");
    }
    ~Test3() {
        cout << "~Test3(): release resources" << endl;
    }
};

void processRecords() {
    try {
        Test2 t;
    } catch (exception &ex) {
        cout << "Test2: " << ex.what() << endl;
    }
    try {
        Test3 t;
    } catch (exception &ex) {
        cout << "Test3: " << ex.what() << endl;
    }
}

int testing(int x) {
    throw x;
}
int sum(int x, int y) noexcept(noexcept(testing(x))) {
    testing(x);
    return x + y;
}
int testing2(int x) noexcept {
}
int sum2(int x, int y) noexcept(noexcept(testing2(x))) {
    testing2(x);
    return x + y;
}

int main() {
    try {
        processRecords(-1);
        std::cout << "record processing is done" << std::endl;
    } catch (runtime_error &ex) {
        cout << ex.what() << endl;
    } catch (out_of_range &ex) {
        cout << ex.what() << endl;
    } catch (exception &ex) {
        // catch all type of exceptions
        cout << ex.what() << endl;
    } catch (...) {
        // catch all exceptions but doesn't have exception information
        cout << "Exception caught" << endl;
    }
    cout << endl;

    try {
        processRecords2(5);
        std::cout << "record processing is done" << std::endl;
    } catch (exception &ex) {
        cout << ex.what() << endl;
    }
    cout << endl;

    try {
        processRecords2(10);
        std::cout << "record processing is done" << std::endl;
    } catch (exception &ex) {
        cout << "main: " << ex.what() << endl;
    }
    cout << endl;

    processRecords();
    cout << endl;

    try {
        sum(3, 5);
    } catch (int x) {
        cout << x << endl;
    }
    cout << endl;

    cout << boolalpha << noexcept(testing(3)) << endl;
    cout << boolalpha << noexcept(testing2(3)) << endl;
    A a;
    cout << boolalpha << noexcept(a.~A()) << endl;

    try {
        sum2(3, 5);
    } catch (int x) {
        cout << x << endl;
    }
    cout << endl;
    return 0;
}
