#include <iostream>

using namespace std;

void pointers() {
    int x = 10;
    cout << &x << endl;
    int *ptr = &x;
    cout << ptr << endl;
    cout << *ptr << endl;

    void *ptv = &x;
    cout << ptv << endl;

    *ptr = 5;
    cout << x << endl;
    int y = *ptr;
    cout << y << endl;

    int *ptn = nullptr;
    cout << ptn << endl;

    cout << endl;
}

void references() {
    int x = 10;
    int &ref = x;
    cout << "x: " << x << endl;
    cout << "ref: " << ref << endl;

    x = 5;
    cout << "x: " << x << endl;
    cout << "ref: " << ref << endl;
    cout << "&x: " << &x << endl;
    cout << "&ref: " << &ref << endl;
    cout << endl;

    int y = 20;
    ref = y;
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;
    cout << "ref: " << ref << endl;

    cout << endl;
}

void swap(int x, int y) {
    int temp = x;
    x = y;
    y = temp;
}

void swap_ptr(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

void swap_ref(int &x, int &y) {
    int temp = x;
    x = y;
    y = temp;
}

void print_ptr(const int *ptr) {
    if (ptr != nullptr)
        cout << *ptr << endl;
}

void print_ref(int &ptr) {
    cout << ptr << endl;
}

void ref_vs_pointer() {
    int a = 5, b = 6;
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    swap(a, b);
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    swap_ptr(&a, &b);
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    swap_ref(a, b);
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << endl;

    print_ptr(&a);
    print_ptr(nullptr);

    print_ref(a);
    cout << endl;
}

int main() {
    // Pointers
    pointers();
    // References
    references();
    // Reference vs pointer
    ref_vs_pointer();
    return 0;
}
