#include <iostream>

using namespace std;
int main() {
    int a1;     // uninitialised
    int a2 = 0; // copy initialisation
    int a3(5);  // direct initialisation
    cout << a1 << endl;
    cout << a2 << endl;
    cout << a3 << endl;

    string s1;
    string s2("C++"); // direct initialisation
    cout << s1 << endl;
    cout << s2 << endl;

    char c1[5]; // uninitialised
    char c2[5] = {'\n'};
    char c3[5] = {'a', 'b', 'c', 'd'}; // aggregate initialisation
    cout << c1 << endl;
    cout << c2 << endl;
    cout << c3 << endl;

    // uniform initialisation
    int b1{}; // value initialisation
    int b2{5}; // direct initialisation
    cout << b1 << endl;
    cout << b2 << endl;

    char d1[6]{};
    char d2[6]{"Hello"};
    cout << d1 << endl;
    cout << d2 << endl;

    int *p1 = new int{};
    cout << p1 << endl;

    char *p2 = new char[6]{};
    char *p3 = new char[6]{"Hello"};
    cout << p2 << endl;
    cout << p3 << endl;

    return 0;
}
