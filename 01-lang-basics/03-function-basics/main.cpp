#include <iostream>
#include "mymath.h"
using namespace std;

// prototype or declaration
int add(int x, int y);

int main() {
    add(2, 3);
    alt_add(5, 6);
    return 0;
}

// definition
int add(int x, int y) {
    int sum = x + y;
    cout << "Sum is: " << sum << endl;
    return sum;
}
