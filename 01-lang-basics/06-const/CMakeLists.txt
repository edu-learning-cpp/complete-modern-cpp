cmake_minimum_required(VERSION 3.17)
project(06_const)

set(CMAKE_CXX_STANDARD 17)

add_executable(06_const main.cpp)