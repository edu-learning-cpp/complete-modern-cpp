#include <iostream>

using namespace std;

void print(int *ptr) {
    cout << *ptr << endl;
    *ptr = 5;
}

void printc(const int *ptr) {
    cout << *ptr << endl;
//    *ptr = 5; // is not possible to modify parameter in this function
}

void print_ref(int &ref) {
    cout << ref << endl;
    ref = 5;
}

void print_refc(const int &ref) {
    cout << ref << endl;
    // ref = 5; // not possible to change const parameter
}

void const_qualifier() {
    float radius = 5.0;
    float area = 3.1415f * radius * radius;
    float circumference = 3.14f * 2 * radius;
    cout << "Area is: " << area << endl;
    cout << "Circumference is: " << circumference << endl;

    const float PI = 3.14159;
    radius = 6;
    area = PI * radius * radius;
    circumference = PI * 2 * radius;
    cout << "Area is: " << area << endl;
    cout << "Circumference is: " << circumference << endl;
    cout << endl;

    const int CHUNK_SIZE = 512;
    const int *ptr = &CHUNK_SIZE;
    cout << ptr << endl;
    cout << *ptr << endl;
    // *ptr = 1; // cannot change value of constant value
    int x = 10;
    ptr = &x; // but can change pointer address
    cout << ptr << endl;
    cout << *ptr << endl;

    const int *const ptc = &CHUNK_SIZE; // contant pointer
    // *ptc = 1; // cannot chage value
    // ptc = &x; // and cannot change value of contant pointer
    cout << ptc << endl;
    cout << *ptc << endl;
    cout << endl;

    print(&x);
    cout << "main->x: " << x << endl;
    x = 10;
    printc(&x);
    cout << "main->x: " << x << endl;
    cout << endl;

    // const refs
    print_ref(x);
    cout << "main->x: " << x << endl;
    x = 10;
    print_refc(x);
    cout << "main->x: " << x << endl;
    cout << endl;
}

int main() {
    const_qualifier();
    return 0;
}
