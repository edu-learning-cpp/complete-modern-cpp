#include <iostream>

using namespace std;

int sum(int x, int y) {
    return x + y;
}

int main() {
    auto x = 5;
    auto y = 5.6f;
    auto sum1 = x + y;
    cout << sum1 << endl;
    auto sum2 = sum(5, 6);
    cout << sum2 << endl;
    cout << endl;

    static int a = 2;
    cout << a << endl;
    const int b = 10;
    const auto var = x;
    auto &var1 = x;
    cout << var1 << endl;

    auto list = {1, 2, 3, 4};
    return 0;
}
