#include <iostream>

int main() {
    using namespace std;
    int i;
    cout << i << endl;
    i = 5;
    cout << i << endl;
    int j {6}; // uniform initialisation since C++ 11
    cout << j << endl;

    char ch = 'a';
    cout << ch << endl;
    bool flag = true;
    cout << "true = " << flag << endl;
    bool flag0 = 0;
    cout << "bool from 0 = " << flag0 << endl;
    bool flag1 = 1;
    cout << "bool from 1 = " << flag1 << endl;
    float f = 1.283f;
    cout << f << endl;
    double d = 543.678;
    cout << d << endl;

    // array
    int arr[5];
    cout << arr << endl;
    int arr1[5] = {1, 2, 3, 4, 5};
    cout << arr1 << endl;
    int arr2[5] {1, 2, 3, 4, 5}; // uniform initialisation
    cout << arr2 << endl;

    return 0;
}
