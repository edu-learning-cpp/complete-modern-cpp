//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "inline_functions.h"
using namespace std;

int square(int x) {
    return x * x;
}

#define square_m(x) x * x

inline int square_f(int x) {
    return x * x;
}

void inline_functions() {
    int val = 5;
    int result = square(val);
    cout << result << endl;
    cout << square_m(6) << endl;
    cout << square_m(val + 1) << endl;
    cout << square_f(val) << endl;
    cout << square_f(val + 1) << endl;
    cout << endl;
}
