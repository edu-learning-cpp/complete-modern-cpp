#include "function_overloading.h"
#include "default_arguments.h"
#include "inline_functions.h"
#include "function_pointers.h"
#include "namespaces.h"

int main() {
    // Function overloading
    function_overloading();
    // Default function arguments
    default_arguments();
    // Inline functions
    inline_functions();
    // Function pointers
    function_pointers();
    // Namespace
    namespaces();
    return 0;
}
