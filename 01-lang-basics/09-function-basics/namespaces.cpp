//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "namespaces.h"
using std::cout, std::endl;

namespace avg {
    float calculate(float x, float y) {
        return (x * y) / 2;
    }
}

namespace basic {
    float calculate(float x, float y) {
        return x * y;
    }
}

namespace sort {
    void quick_sort() {}
    void insert_sort() {}
    void merge_sort() {}
    namespace comparison {
        void less() {}
        void greater() {}
    }
}

namespace {
    // this function will not be visible outside of this source file
    void internal_func() {
        cout << "internal function inside anonymous namespace" << endl;
    }
}

void namespaces() {
    cout << avg::calculate(3.9f, 8.2f) << endl;
    cout << basic::calculate(3.9f, 8.2f) << endl;
    cout << endl;

    using namespace sort::comparison;
    less();
    greater();
    sort::quick_sort();
    sort::insert_sort();
    sort::merge_sort();

    internal_func();
    cout << endl;
}
