//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "function_overloading.h"
using namespace std;

int add(int a, int b) {
    return a + b;
}

double add(double a, double b) {
    return a + b;
}

void print(int x) {
    cout << x << endl;
}

void print(int *x) {
    cout << *x << endl;
}

void print(const int *x) {
    cout << *x << endl;
}

void print(double x) {
    cout << x << endl;
}

void function_overloading() {
    int result = add(3, 5);
    print(result); // invoke (int x)
    print(add(3.1, 6.2));
    int x = 5;
    print(&x); // invoke (int* x)
    const int y = 6;
    print(&y); // invoke (const int* x)
    cout << endl;
}
