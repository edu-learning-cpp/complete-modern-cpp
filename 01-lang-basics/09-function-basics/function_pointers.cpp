//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "function_pointers.h"
using namespace std;

void print(int count, char ch) {
    for (int i = 0; i < count; ++i) {
        cout << ch;
    }
    cout << endl;
}

void end_message() {
    cout << "End for program" << endl;
}

void function_pointers() {
    print(5, '#');
    void (*pfn)(int, char) = &print;
    (*pfn)(8, '=');
    pfn(5, '-');
    atexit(end_message);
    cout << "end of function" << endl;
    cout << endl;
}
