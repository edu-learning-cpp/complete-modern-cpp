//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "default_arguments.h"
using namespace std;

void createWindow(const char* title, int x = 10, int y = 10, int width = 640, int height = 480) {
    cout << "Title : " << title << endl;
    cout << "x : " << x << endl;
    cout << "y : " << y << endl;
    cout << "width : " << width << endl;
    cout << "height : " << height << endl;
}

void default_arguments() {
    createWindow("Notepad", 150, 200, 500, 600);
    cout << endl;
    createWindow("Notepad++");
    cout << endl;
    createWindow("Sublime", 50, 50);
    cout << endl;
}
