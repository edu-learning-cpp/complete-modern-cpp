#include <iostream>

using namespace std;

int main() {
    int arr[] = {1, 2, 3, 4, 5};
    for (int i = 0; i < 5; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;

    // range based loop
    for (const auto &x : arr) {
        cout << x << " ";
    }
    cout << endl;
    for (const auto &x : {9, 8, 7, 6, 5}) {
        cout << x << " ";
    }
    cout << endl;

    int *beg = std::begin(arr);  // same as &arr[0];
    int *end = std::end(arr);        // same as &arr[5];
    while (beg != end) {
        cout << *beg << " ";
        ++beg;
    }
    cout << endl;

    auto begin = std::begin(arr);
    auto ending = std::end(arr);
    for (; begin != ending; ++begin) {
        auto v = *begin;
        cout << v << " ";
    }
    cout << endl;
    return 0;
}
