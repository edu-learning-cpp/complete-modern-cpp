#include <iostream>
#include <memory>
#include "Integer.h"
#include "weak_pointers.h"

using namespace std;

void display(Integer *p) {
    if (!p) {
        return;
    }
    cout << p->geValue() << endl;
}

Integer *getPointer(int value) {
    auto *p = new Integer(value);
    return p;
}

void operate(int value) {
    Integer *p = getPointer(value);
    if (p == nullptr) {
        p = new Integer(value);
    }
    p->setValue(100);
    display(p);
    delete p;
    p = nullptr;
    p = new Integer{};
    *p = __LINE__;
    display(p);
    delete p;
}

void store(std::unique_ptr<Integer> p) {
    cout << "Storing data into a file: " << p->geValue() << endl;
}

void operateSmartPointer(int value) {
    cout << endl;
    unique_ptr<Integer> p{getPointer(value)};
    if (p == nullptr) {
        p = std::make_unique<Integer>(value);
    }
    p->setValue(100);
    display(p.get());
    p = std::make_unique<Integer>();
    *p = __LINE__;
    display(p.get());
//    store(p); // this will not compile, cannot use used copy
    store(std::move(p)); // but can move
}

class Project {
public:
    ~Project() {
        cout << "~Project()" << endl;
    }
};

class EmployeeOld {
    Project *m_pProject;
public:
    void setProject(Project *p) {
        m_pProject = p;
    }
    Project *getProject() const {
        return m_pProject;
    }
    ~EmployeeOld() {
        cout << "~EmployeeOld()" << endl;
    }
};

EmployeeOld *assignProjectOld() {
    Project *p = new Project;
    EmployeeOld *e = new EmployeeOld;
    e->setProject(p);
    return e;
}

class Employee {
    shared_ptr<Project> m_pProject;
public:
    void setProject(shared_ptr<Project> p) {
        m_pProject = p;
    }
    shared_ptr<Project> getProject() const {
        return m_pProject;
    }
    ~Employee() {
        cout << "~Employee()" << endl;
    }
};

shared_ptr<Employee> assignProject() {
    shared_ptr<Project> p{new Project};
    Employee *e = new Employee;
    e->setProject(p);
    return shared_ptr<Employee>(e);
}

void smartPointers3() {
    cout << endl;
    Project *p = new Project;
    EmployeeOld e1, e2;
    e1.setProject(p);
    e2.setProject(p);
    delete p;

    auto sp = assignProject();
}

int main() {
    operate(5);
    operateSmartPointer(5);
    smartPointers3();
    weak_pointers();
    return 0;
}
