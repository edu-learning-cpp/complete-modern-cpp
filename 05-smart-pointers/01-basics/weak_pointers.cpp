//
// Created by Eduard Luhtonen on 11.08.20.
//
#include <iostream>
#include <random>
#include "weak_pointers.h"
using namespace std;

class PrinterRaw {
    int *m_pValue{};
public:
    void setValue(int *p) {
        m_pValue = p;
    }
    void print() {
        cout << "Value is: " << *m_pValue << endl;
    }
};

class PrinterShared {
    shared_ptr<int> m_pValue{};
public:
    void setValue(shared_ptr<int> p) {
        m_pValue = p;
    }
    void print() {
        cout << "Ref count: " << m_pValue.use_count() << endl;
        cout << "Value is: " << *m_pValue << endl;
    }
};

class PrinterWeak {
    weak_ptr<int> m_pValue{};
public:
    void setValue(weak_ptr<int> p) {
        m_pValue = p;
    }
    void print() {
        cout << "Ref count: " << m_pValue.use_count() << endl;
        if (m_pValue.expired()) {
            cout << "Resource is no longer available" << endl;
            return;
        }
        auto sp = m_pValue.lock();
        cout << "Value is: " << *sp << endl;
    }
};

void use_raw_pointer(int v) {
    cout << endl;
    PrinterRaw prn;
    int num{v};
    int *p = new int{num};
    prn.setValue(p);
    prn.print();
    if (*p > 10) {
        cout << "deleting pointer..." << endl;
        delete p;
        p = nullptr;
    }

    prn.print();
    delete p;
}

void use_shared_pointer(int v) {
    cout << endl;
    PrinterShared prn;
    int num{v};
    shared_ptr<int> p{new int{num}};
    prn.setValue(p);
    prn.print();
    if (*p > 10) {
        cout << "deleting pointer..." << endl;
        p = nullptr;
    }
    prn.print();
}

void use_weak_pointer(int v) {
    cout << endl;
    PrinterWeak prn;
    int num{v};
    shared_ptr<int> p{new int{num}};
    prn.setValue(p);
    prn.print();
    if (*p > 10) {
        cout << "deleting pointer..." << endl;
        p = nullptr;
    }
    prn.print();
}

class Employee1;
class Project1 {
public:
    Employee1 *m_emp;
    Project1() {
        cout << "Project1()" <<endl;
    }
    ~Project1() {
        cout << "~Project1()" <<endl;
    }
};
class Employee1 {
public:
    Project1 *m_prj;
    Employee1() {
        cout << "Employee1()" <<endl;
    }
    ~Employee1() {
        cout << "~Employee1()" <<endl;
    }
};

class Employee2;
class Project2 {
public:
    shared_ptr<Employee2> m_emp;
    Project2() {
        cout << "Project2()" <<endl;
    }
    ~Project2() {
        cout << "~Project2()" <<endl;
    }
};
class Employee2 {
public:
    shared_ptr<Project2> m_prj;
    Employee2() {
        cout << "Employee2()" <<endl;
    }
    ~Employee2() {
        cout << "~Employee2()" <<endl;
    }
};

class Employee3;
class Project3 {
public:
    shared_ptr<Employee3> m_emp;
    Project3() {
        cout << "Project3()" <<endl;
    }
    ~Project3() {
        cout << "~Project3()" <<endl;
    }
};
class Employee3 {
public:
    weak_ptr<Project3> m_prj;
    Employee3() {
        cout << "Employee3()" <<endl;
    }
    ~Employee3() {
        cout << "~Employee3()" <<endl;
    }
};

void using_smart_pointers() {
    cout << endl;
    Employee1 *emp1 = new Employee1{};
    Project1 *prj1 = new Project1{};

    emp1->m_prj = prj1;
    prj1->m_emp = emp1;

    delete emp1;
    delete prj1;

    cout << endl;
    shared_ptr<Employee2> emp2{new Employee2{}};
    shared_ptr<Project2> prj2{new Project2{}};

    // this leads to circular reference
    // memory will not be released
    emp2->m_prj = prj2;
    prj2->m_emp = emp2;

    // solving circular reference problem with weak pointer
    cout << endl;
    shared_ptr<Employee3> emp3{new Employee3{}};
    shared_ptr<Project3> prj3{new Project3{}};

    emp3->m_prj = prj3;
    prj3->m_emp = emp3;
}

void weak_pointers() {
    cout << endl;
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distr(10, 15);

    use_raw_pointer(distr(gen));

    use_shared_pointer(distr(gen));

    use_weak_pointer(distr(gen));

    using_smart_pointers();
}
