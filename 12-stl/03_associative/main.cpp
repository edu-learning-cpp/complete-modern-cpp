#include <iostream>
#include <set>
#include <functional>
#include <map>

using std::cout, std::endl;

void mySet() {
    cout << "*** std::set ***" << endl;
    std::set<int> s{8, 2, 0, 9, 5};
    s.insert(1);
    s.insert(6);

    auto itr = s.begin();
    while (itr != s.end()) {
        cout << *itr++ << " ";
    }
    cout << endl;

    s.erase(0);
    s.erase(s.begin());

    itr = s.find(9);
    if (itr != s.end()) {
        cout << "Element found" << endl;
    } else {
        cout << "Not found" << endl;
    }
    cout << endl;

    std::set<int, std::greater<>> s2{8, 2, 0, 9, 5};
    itr = s2.begin();
    s2.insert(6);
    s2.insert(6);
    s2.insert(6);
    while (itr != s2.end()) {
        cout << *itr++ << " ";
    }
    cout << endl;
}

void myMultiSet() {
    cout << "*** std::multiset ***" << endl;
    std::multiset<int, std::greater<>> s{8, 2, 0, 9, 5};
    auto itr = s.begin();
    s.insert(6);
    s.insert(6);
    s.insert(6);
    while (itr != s.end()) {
        cout << *itr++ << " ";
    }
    cout << endl;

    auto found = s.equal_range(6);
    while (found.first != found.second) {
        cout << *found.first++ << " ";
    }
    cout << endl;
}

void myMap() {
    cout << "*** std::map ***" << endl;
    std::map<int, std::string> m{
            {1, "Superman"},
            {2, "Batman"},
            {3, "Green Lantern"}
    };
    m.insert(std::pair<int, std::string>(8, "Aquaman"));
    m.insert(std::make_pair(6, "Wonder Women"));
    m.insert(std::make_pair(6, "Power Girl"));

    m[0] = "Flash";
    auto itr = m.begin();
    cout << itr->first << ": " << itr->second << endl;

    m[0] = "Kid Flash";
    for (const auto& x : m) {
        cout << x.first << ": " << x.second << endl;
    }

    m.erase(0);

    itr = m.find(1);
    if (itr != m.end()) {
        cout << "Found: " << itr->second << endl;
    } else {
        cout << "Not found" << endl;
    }
    cout << m[1] << endl;
    cout << "m[10]: '" << m[10] << "'" << endl;
}

void myMultiMap() {
    cout << "*** std::multimap ***" << endl;
    std::multimap<int, std::string> m{
            {1, "Superman"},
            {2, "Batman"},
            {3, "Green Lantern"}
    };
    m.insert(std::pair<int, std::string>(8, "Aquaman"));
    m.insert(std::make_pair(6, "Wonder Women"));
    m.insert(std::make_pair(6, "Power Girl"));

    auto itr = m.begin();
    m.erase(0);

    for (const auto& x : m) {
        cout << x.first << ": " << x.second << endl;
    }

    itr = m.find(1);
    if (itr != m.end()) {
        cout << "Found: " << itr->second << endl;
    } else {
        cout << "Not found" << endl;
    }

    auto found = m.equal_range(6);
    while(found.first != found.second) {
        cout << found.first->first << ": " << found.first->second << endl;
        found.first++;
    }
}

int main() {
    mySet();
    cout << endl;

    myMultiSet();
    cout << endl;

    myMap();
    cout << endl;

    myMultiMap();
    cout << endl;
    return 0;
}
