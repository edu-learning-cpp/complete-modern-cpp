#include <iostream>
#include <array>
using std::cout, std::endl;

void myArray() {
    std::array<int, 5> arr{3, 1, 8, 5, 9};
    for (int i = 0; i < arr.size(); ++i) {
        arr[i] = i;
    }
    auto iter = arr.begin();
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
}

int main() {
    myArray();
    return 0;
}
