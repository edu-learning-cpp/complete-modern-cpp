#include <iostream>
#include <vector>
#include <set>
#include <unordered_set>
#include <algorithm>
using std::cout, std::endl;

class Employee {
    std::string name;
    int id;
    std::string progLang;
public:
    Employee(const std::string &name, int id, const std::string &pl) : name(name), id(id), progLang(pl) {}

    const std::string &getName() const {
        return name;
    }

    int getId() const {
        return id;
    }

    const std::string &getProgLang() const {
        return progLang;
    }

    bool operator<(const Employee &e) const {
        return id < e.getId();
    }
};

struct EmpCompare {
    bool operator()(const Employee &e1, const Employee &e2) const {
        return e1.getId() < e2.getId();
    }
};

struct EmpIds {
    std::vector<int> ids;
    void operator()(const Employee &e) {
        if (e.getProgLang() == "C++") {
            ids.emplace_back(e.getId());
        }
    }
};
void userDefined() {
    // sort
    std::vector<Employee> v{
        Employee("John", 101, "C++"),
        Employee("Bob", 202, "Java"),
        Employee("Joye", 200, "C++")
    };
    std::sort(v.begin(), v.end()); // default sort by id
    cout << "==== Sorted by ID" << endl;
    for (const auto &x : v) {
        cout << x.getId() << ":" << x.getName() << ":" << x.getProgLang() << endl;
    }
    std::sort(v.begin(), v.end(), [](const auto &e1, const auto &e2) { // sort by name
        return e1.getName() < e2.getName();
    });
    cout << "==== Sorted by name" << endl;
    for (const auto &x : v) {
        cout << x.getId() << ":" << x.getName() << ":" << x.getProgLang() << endl;
    }
    cout << endl;

    std::set<Employee, EmpCompare> s{
        Employee("John", 101, "C++"),
        Employee("Bob", 202, "Java"),
        Employee("Joe", 200, "C++")
    };
    for (const auto &x : s) {
        cout
        << "Id:" << x.getId()
        << " | Name:" << x.getName()
        << " | Language:" << x.getProgLang() << endl;
    }
    cout << endl;

    // count
    int cppCount{};
    for (const auto &e : v) {
        if (e.getProgLang() == "C++") {
            cppCount++;
        }
    }
    cout << "Count: " << cppCount << endl;

    // this will require to overload == operator
//    cppCount = std::count(v.begin(), v.end(), Employee("", 0, "C++"));
    cppCount = std::count_if(v.begin(), v.end(), [](const auto &e) {
        return e.getProgLang() == "C++";
    });
    cout << "Count: " << cppCount << endl;

    // find
    auto itr = std::find_if(v.begin(), v.end(), [](const auto &e) {
        return e.getProgLang() == "Java";
    });
    if (itr != v.end()) {
        cout << "Found: " << itr->getName() << " is a Java programmer" << endl;
    }
    cout << endl;

    // for each
    std::for_each(v.begin(), v.end(), [](const auto &e) {
        cout << e.getName() << endl;
    });

    auto foundIds = std::for_each(v.begin(), v.end(), EmpIds());
    for (int id : foundIds.ids) {
        cout << "Id: " << id << endl;
    }
}

int main() {
    userDefined();
    cout << endl;
    return 0;
}
