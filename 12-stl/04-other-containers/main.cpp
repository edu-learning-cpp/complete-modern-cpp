#include <iostream>
#include <unordered_set>
#include <unordered_map>

using std::cout, std::endl;

void unordered_set() {
    cout << "*** unordered_set ***" << endl;
    std::unordered_set<std::string> coll;
    coll.insert("Hulk");
    coll.insert("Batman");
    coll.insert("Green Lantern");
    coll.insert("The Flash");
    coll.insert("Wonder Women");
    coll.insert("Iron man");
    coll.insert("Iron man");
    coll.insert("Iron man");
    coll.insert("Wolverine");
    coll.insert("Dr. Strange");
    coll.insert("Hawkman");
    coll.insert("Robin");
    coll.insert("Catwoman");
    coll.insert("Joker");
    for (const auto x : coll) {
        cout << "Bucket #:" << coll.bucket(x) << " contains: " << x << endl;
    }

    cout << "Bucket count: " << coll.bucket_count() << endl;
    cout << "Number of elements: " << coll.size() << endl;
    cout << "Load factor: " << coll.load_factor() << endl;
}

void unordered_multiset() {
    cout << "*** unordered_multiset ***" << endl;
    std::unordered_multiset<std::string> coll;
    coll.insert("Hulk");
    coll.insert("Batman");
    coll.insert("Green Lantern");
    coll.insert("The Flash");
    coll.insert("Wonder Women");
    coll.insert("Iron man");
    coll.insert("Iron man");
    coll.insert("Iron man");
    coll.insert("Wolverine");
    coll.insert("Dr. Strange");
    coll.insert("Hawkman");
    coll.insert("Robin");
    coll.insert("Catwoman");
    coll.insert("Joker");
    for (const auto x : coll) {
        cout << "Bucket #:" << coll.bucket(x) << " contains: " << x << endl;
    }

    cout << "Bucket count: " << coll.bucket_count() << endl;
    cout << "Number of elements: " << coll.size() << endl;
    cout << "Load factor: " << coll.load_factor() << endl;
}

void unordered_map() {
    cout << "*** unordered_map ***" << endl;
    std::unordered_map<std::string, std::string> m;
    m["Batman"] = "Bruce Wayne";
    m["Superman"] = "Clark Kent";
    m["Superman"] = "Clark Kent";
    m["Hulk"] = "Bruce Banner";

    for (const auto &x : m) {
        cout << "Bucket #: " << m.bucket(x.first) << " -> " << x.first << ": " << x.second << endl;
    }
}

void unordered_multimap() {
    cout << "*** unordered_multimap ***" << endl;
    std::unordered_multimap<std::string, std::string> m;
    m.insert(std::make_pair("Batman", "Bruce Wayne"));
    m.insert(std::make_pair("Batman", "Matches Malone"));
    m.insert(std::make_pair("Superman", "Clark Kent"));
    m.insert(std::make_pair("Hulk", "Bruce Banner"));

    for (const auto &x : m) {
        cout << "Bucket #: " << m.bucket(x.first) << " -> " << x.first << ": " << x.second << endl;
    }
}

class Employee {
    std::string name;
    int id;
public:
    Employee(const std::string &name, int id) : name(name), id(id) {}

    [[nodiscard]] const std::string &getName() const {
        return name;
    }

    [[nodiscard]] int getId() const {
        return id;
    }
};
struct EmployeeHash {
    size_t operator()(const Employee &emp) const {
        auto s1 = std::hash<std::string>{}(emp.getName());
        auto s2 = std::hash<int>{}(emp.getId());
        return s1 ^ s2;
    }
};
struct EmpEquality {
    bool operator()(const Employee &e1, const Employee &e2) const {
        return e1.getId() == e2.getId();
    }
};

void hashes() {
    cout << "*** std::hash ***" << endl;
    std::hash<std::string> h;
    cout << "Hash: " << h("hello") << endl;

    std::unordered_set<Employee, EmployeeHash, EmpEquality> coll;
    coll.insert(Employee("John", 123));
    coll.insert(Employee("Bob", 456));
    coll.insert(Employee("Joe", 789));
    for (const auto x : coll) {
        cout << x.getId() << ":" << x.getName() << endl;
    }
}

int main() {
    unordered_set();
    cout << endl;

    unordered_multiset();
    cout << endl;

    unordered_map();
    cout << endl;

    unordered_multimap();
    cout << endl;

    hashes();
    cout << endl;
    return 0;
}
