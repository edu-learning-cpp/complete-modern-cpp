cmake_minimum_required(VERSION 3.17)
project(04_other_containers)

set(CMAKE_CXX_STANDARD 17)

add_executable(04_other_containers main.cpp)