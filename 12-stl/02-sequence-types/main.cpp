#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
using std::cout, std::endl;

void myVector() {
    std::vector<int> coll{1, 2, 3, 4};
    for (int i = 0; i < 5; ++i) {
        coll.emplace_back(i * 10);
        cout << coll.size() << endl;
    }

    coll[0] = 100;
    for (int i = 0; i < coll.size(); ++i) {
        cout << coll[i] << " ";
    }
    cout << endl;

    auto itr = coll.begin();
    while (itr != coll.end()) {
        cout << *itr++ << " ";
    }
    cout << endl;

    coll.insert(coll.begin(), 700);
    coll.erase(coll.end() - 5);
    coll.pop_back();

    for (auto x : coll) {
        cout << x << " ";
    }
    cout << endl;
}

void myDeque() {
    cout << "==== deque ====" << endl;
    std::deque<int> coll;
    for (int i = 0; i < 5; ++i) {
        coll.emplace_back(i * 10);
    }
    for (int i = 0; i < 5; ++i) {
        coll.push_front(i * 10);
    }

    coll[0] = 100;
    for (int i = 0; i < coll.size(); ++i) {
        cout << coll[i] << " ";
    }
    cout << endl;

    auto itr = coll.begin();
    while (itr != coll.end()) {
        cout << *itr++ << " ";
    }
    cout << endl;

    coll.insert(coll.begin(), 700);
    coll.erase(coll.end() - 5);
    coll.pop_back();
    coll.pop_front();

    for (auto x : coll) {
        cout << x << " ";
    }
    cout << endl;
}

void myList() {
    cout << "==== list ====" << endl;
    std::list<int> coll;
    for (int i = 0; i < 5; ++i) {
        coll.push_back(i * 10);
    }
    auto itr = coll.begin();
    while (itr != coll.end()) {
        cout << *itr++ << " ";
    }
    cout << endl;
    itr = coll.begin();
    coll.insert(itr, 800);
    coll.erase(itr);
}
void myForwardList() {
    cout << "==== forward_list ====" << endl;
    std::forward_list<int> coll;
    for (int i = 0; i < 10; ++i) {
        coll.push_front(i);
    }
    for (auto x : coll) {
        cout << x << " ";
    }
    cout << endl;
    coll.insert_after(coll.begin(), 10);
    coll.erase_after(coll.begin());
}

int main() {
    myVector();
    cout << endl;

    myDeque();
    cout << endl;

    myList();
    cout << endl;

    myForwardList();
    cout << endl;
    return 0;
}
