#include <iostream>
using std::cout, std::endl;

void intro_lambdas() {
    []() {
        cout << "Welcome to lambda expressions" << endl;
    }();
    auto fn = []() {
        cout << "Welcome to lambda expressions again" << endl;
    };
    fn();
    cout << typeid(fn).name() << endl;
}

struct Unnamed {
    int operator()(int x, int y) const {
        return x + y;
    }
};

template<typename T>
struct Unnamed_T {
    T operator()(T x, T y) const {
        return x + y;
    }
};

void lambdas_internals() {
    auto sum = [](int x, int y) {
        return x + y;
    };
    cout << "Sum is: " << sum(5, 2) << endl;

    Unnamed n;
    cout << n(5, 6) << endl;
    Unnamed_T<double> t;
    cout << t(5.6, 2.3) << endl;

    // generic lambda since C++14
    auto sumg = [](auto x, auto y) {
        return x + y;
    };
    cout << "Sum is: " << sumg(5, 2) << endl;
    cout << "Sum is: " << sumg(5.6, 2.3) << endl;
}

int main() {
    intro_lambdas();
    cout << endl;

    lambdas_internals();
    cout << endl;

    return 0;
}
