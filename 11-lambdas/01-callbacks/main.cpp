#include <iostream>

using std::cout, std::endl;

using Comparator = bool (*)(int, int);

template<typename T, int size>
void sort(T(&arr)[size], Comparator comp) {
    for (int i = 0; i < size - 1; ++i) {
        for (int j = 0; j < size - 1; ++j) {
            // if (arr[j] > arr[j + 1]) {
            if (comp(arr[j], arr[j + 1])) {
                T temp = std::move(arr[j]);
                arr[j] = std::move(arr[j + 1]);
                arr[j + 1] = std::move(temp);
            }
        }
    }
}

bool comp_asc(int x, int y) {
    return x > y;
}

bool comp_desc(int x, int y) {
    return x < y;
}

void function_pointers() {
    int arr[]{6, 8, 1, 5, 4};
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    sort(arr, comp_asc);
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    sort(arr, comp_desc);
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
}

struct Comp2 {
    bool operator()(int x, int y) {
        return x > y;
    }
};

template<typename T, int size, typename Comparator2>
void sort2(T(&arr)[size], Comparator2 comp) {
    for (int i = 0; i < size - 1; ++i) {
        for (int j = 0; j < size - 1; ++j) {
            if (comp(arr[j], arr[j + 1])) {
                T temp = std::move(arr[j]);
                arr[j] = std::move(arr[j + 1]);
                arr[j + 1] = std::move(temp);
            }
        }
    }
}

void function_objects() {
    cout << std::boolalpha << comp_asc(3, 5) << endl;
    Comp2 comp;
    cout << std::boolalpha << comp(3, 5) << endl;

    int arr[]{6, 8, 1, 5, 4};
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    sort2(arr, comp_desc);
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    sort2(arr, comp);
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
}

int main() {
    function_pointers();
    cout << endl;

    function_objects();
    cout << endl;

    return 0;
}
