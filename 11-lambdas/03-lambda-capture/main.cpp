#include <iostream>
#include <fstream>

using std::cout, std::endl;

template<typename T, int size, typename Comparator>
void sort(T(&arr)[size], Comparator comp) {
    for (int i = 0; i < size - 1; ++i) {
        for (int j = 0; j < size - 1; ++j) {
            if (comp(arr[j], arr[j + 1])) {
                T temp = std::move(arr[j]);
                arr[j] = std::move(arr[j + 1]);
                arr[j + 1] = std::move(temp);
            }
        }
    }
}

template<typename T, int size, typename Callback>
void forEach(T(&arr)[size], Callback operation) {
    for (int i = 0; i < size; ++i) {
        operation(arr[i]);
    }
}

void callback_with_lambda() {
    int arr[]{1, 6, 8, 5, 4};
    forEach(arr, [](auto x) {
        cout << x << " ";
    });
    cout << endl;

    int offset = 5;
    forEach(arr, [offset](auto &x) { // need to add variable into capture list
        x += offset;
    });
    forEach(arr, [](auto x) {
        cout << x << " ";
    });
    cout << endl;
    // to be able modify offset inside lambda it must be made mutable
    forEach(arr, [offset](auto &x) mutable {
        x += offset;
        ++offset;
    });
    forEach(arr, [](auto x) {
        cout << x << " ";
    });
    cout << endl;
    int sum{};
    forEach(arr, [&sum](auto x) {
        sum += x;
    });
    cout << "Sum is: " << sum << endl;
}

void lambda_capture_list() {
    int arr[]{1, 6, 8, 5, 4};
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    sort(arr, [](auto x, auto y) {
        return x > y;
    });
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    auto comp = [](auto x, auto y) {
        return x < y;
    };
    sort(arr, comp);
    for (auto x : arr) {
        cout << x << " ";
    }
    cout << endl;
    cout << endl;

    callback_with_lambda();
    cout << endl;
}

class Product {
    std::string name;
    float price;
public:
    Product(const std::string &n, float p) : name(n), price(p) {}
    void assignFinalPrice() {
        float taxes[] {12, 5, 5};
        float basePrice{price};
        forEach(taxes, [basePrice, this](float tax) {
            float taxedPrice = basePrice * tax / 100;
            price += taxedPrice;
        });
    }
    float getPrice() const {
        return price;
    }
};

void capture_this() {
    Product p{"Watch", 500};
    cout << "Price before taxes: " << p.getPrice() << endl;
    p.assignFinalPrice();
    cout << "Final price: " << p.getPrice() << endl;

    [](int x) {
        x *= 2;
        [](int x) {
            cout << x << endl;
        }(x);
    }(5);

    atexit([]() {
        cout << "Program is exiting..." << endl;
    });
}

void generalised_capture() {
    int x{5};
    auto f = [y = x](int arg) {
        return y + arg;
    };
    cout << f(5) << endl;

    std::ofstream out{"file.txt"};
    auto write = [out = std::move(out)] (int x) mutable {
        out << x;
    };
    write(200);
}

int main() {
    lambda_capture_list();
    cout << endl;

    capture_this();
    cout << endl;

    generalised_capture();
    cout << endl;
    return 0;
}
