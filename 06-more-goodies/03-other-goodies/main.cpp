#include <iostream>
#include <initializer_list>
#include <cassert>
#include <vector>

using namespace std;

int getNumber() {
    return 42;
}

constexpr int getConstNumber() {
    return 10;
}

constexpr int add(int x, int y) {
    return x + y;
}

constexpr int max(int x, int y) {
    if (x > y) {
        return x;
    } else {
        return y;
    }
//    return x > y ? x : y;
}

void using_constexpr() {
//    constexpr int i = getNumber(); // is not possible
    constexpr int i = getConstNumber();
    int arr[i];

    const int j = getNumber();
    int arr1[j];

    int x = getConstNumber();
    cout << x << endl;

    constexpr int sum = add(3, 5);
//    constexpr int sum = add(x, 5); // accepts only literals
    cout << sum << endl;

    int sum2 = add(4, 5);
    cout << sum2 << endl;
    cout << endl;
}

class Bag {
    int arr[10];
    int m_size{};
public:
    Bag(initializer_list<int> values) {
        assert(values.size() < 10);
        auto it = values.begin();
        while (it != values.end()) {
            add(*it);
            ++it;
        }
    }

    void add(int value) {
        assert(m_size < 10);
        arr[m_size++] = value;
    }

    void remove() {
        --m_size;
    }

    int operator[](int index) {
        return arr[index];
    }

    int getSize() const {
        return m_size;
    }
};

void print1(initializer_list<int> values) {
    auto it = values.begin();
    while (it != values.end()) {
        cout << *it++ << " ";
    }
    cout << endl;
}

void print2(initializer_list<int> values) {
    for (auto x : values) {
        cout << x << " ";
    }
    cout << endl;
}

void use_initializer_list() {
    initializer_list<int> data = {1, 2, 3, 4};
    auto values = {1, 2, 3, 4};

    Bag b{};
    b.add(5);
    b.add(6);
    b.add(3);
    for (int i = 0; i < b.getSize(); ++i) {
        cout << b[i] << " ";
    }
    cout << endl;

    Bag b1{1, 2, 3, 4, 5};
    for (int i = 0; i < b1.getSize(); ++i) {
        cout << b1[i] << " ";
    }
    cout << endl;

    print1({1, 2, 3, 4});
    print2({4, 3, 2, 1});
    cout << endl;
}

void use_vector() {
    int arr[10]; // fixed size and cannot be expanded
    int *prt = new int[10]; // dynamic array with manual memory management
    for (int i = 0; i < 10; ++i) {
        prt[i] = i * 10;
    }
    // to add more elements have to increase the size of the array

    // std::vector is a dynamic array with automated memory management
    vector<int> data{1, 2, 3};
    data.emplace_back(4);
    for (int i = 0; i < 5; ++i) {
        data.emplace_back(i * 10);
    }
    // access element
    data[0] = 100;
    for (int i = 0; i < data.size(); ++i) {
        cout << data[i] << " ";
    }
    cout << endl;
    for (auto x : data) {
        cout << x << " ";
    }
    cout << endl;

    auto it = data.begin();
    cout << *it;
    cout << endl;

    // delete element
    it = data.begin();
    data.erase(it);
    for (auto x : data) {
        cout << x << " ";
    }
    cout << endl;

    // insert element at specific position
    it = data.begin() + 5;
    data.insert(it, 500);
    for (auto x : data) {
        cout << x << " ";
    }
    cout << endl;
}

union Test {
    int x;
    double d;
    char ch;

    Test() : ch{'a'} {
        cout << __FUNCTION__ << endl;
    }

    ~Test() {
        cout << __FUNCTION__ << endl;
    }
};

void use_union() {
    cout << endl;
    cout << sizeof(Test) << endl;
    Test t;
    cout << t.ch << endl;

    t.x = 1000;
    cout << t.ch << endl;
}

struct A {
    A() {
        std::cout << __FUNCTION__ << std::endl;
    }
    ~A() {
        std::cout << __FUNCTION__ << std::endl;
    }
    A(const A &other) {
        std::cout << __FUNCTION__ << std::endl;
    }
    A(A &&other) noexcept {
        std::cout << __FUNCTION__ << std::endl;
    }
    A &operator=(const A &other) {
        std::cout << __FUNCTION__ << std::endl;
        if (this == &other)
            return *this;
        return *this;
    }
    A &operator=(A &&other) noexcept {
        std::cout << __FUNCTION__ << std::endl;
        if (this == &other)
            return *this;
        return *this;
    }
};

struct B {
    B() {
        std::cout << __FUNCTION__ << std::endl;
    }
    ~B() {
        std::cout << __FUNCTION__ << std::endl;
    }
    B(const B &other) {
        std::cout << __FUNCTION__ << std::endl;
    }
    B(B &&other) noexcept {
        std::cout << __FUNCTION__ << std::endl;
    }
    B &operator=(const B &other) {
        std::cout << __FUNCTION__ << std::endl;
        if (this == &other)
            return *this;
        return *this;
    }
    B &operator=(B &&other) noexcept {
        std::cout << __FUNCTION__ << std::endl;
        if (this == &other)
            return *this;
        return *this;
    }
    virtual void Foo() {}
};

union UDT {
    A a;
    B b;
    string s;
    UDT() {
        std::cout << __FUNCTION__ << std::endl;
    }
    ~UDT() {
        std::cout << __FUNCTION__ << std::endl;
    }
};

void use_udt() {
    cout << endl;
    UDT udt;
    //udt.a = A{}; // incorrect
    //new(&udt.s) string{"Hello World"};

    new(&udt.a) A{};
    udt.a.~A();
}

int main() {
    using_constexpr();
    use_initializer_list();
    use_vector();
    use_union();
    use_udt();
    return 0;
}
