#include <iostream>
#include <cstring>
#include <sstream>
using namespace std;

const char *combine1(const char *pFirst, const char *pLast) {
    char fullName[20];
    strcpy(fullName, pFirst);
    strcat(fullName, pLast);
    return fullName; // return pointer to local variable
}

const char *combine2(const char *pFirst, const char *pLast) {
    char *fullName = new char[strlen(pLast) + strlen(pLast) + 1];
    strcpy(fullName, pFirst);
    strcat(fullName, pLast);
    return fullName; // return pointer to local variable
}

void raw_strings() {
    char first[10] = "Edu ";
    char last[10] = "Finn";

    const char *pFullName1 = combine1(first, last);
    cout << "combine1: " << pFullName1 << endl;

    const char *pFullName2 = combine2(first, last);
    cout << "combine1: " << pFullName2 << endl;
    delete [] pFullName2;
}

void std_string() {
    // Initialise and assign
    string s = "Hey";
    s = "Hello";

    // access
    cout << s << endl;
    s[0] = 'W';
    cout << s << endl;
    char ch = s[0];
    cout << ch << endl;

    // size
    cout << s.length() << endl;

    // insert & concatenate
    string s1{"Hello"}, s2{"World"};
    s = s1 + s2;
    cout << s << endl;
    s += s1;
    cout << s << endl;
    s.insert(5, "Welt");
    cout << s << endl;

    // comparison
    if (s1 != s2) {
        cout << "strings are not equal" << endl;
    }
    if (s1 > s2) {
        cout << "s1 '" << s1 << "' is greater and s2 '" << s2 << "'" << endl;
    }
    if (s1 < s2) {
        cout << "s1 '" << s1 << "' is less and s2 '" << s2 << "'" << endl;
    }

    // removal
    s.erase(0, 5);
    cout << s << endl;
    s.clear();
    cout << "'" << s << "'" << endl;

    // search
    s = s1 + " " + s2;
    cout << s.find(' ') << endl;
    cout << s.find("World") << endl;
    cout << s.find("Hello") << endl;
    cout << s.find("Welt") << endl;
    auto pos = s.find("Welt");
    if (pos != string::npos) {
        cout << "find at position: " << pos << endl;
    } else {
        cout << "not found" << endl;
    }
    pos = s.find("World");
    if (pos != string::npos) {
        cout << "find at position: " << pos << endl;
    } else {
        cout << "not found" << endl;
    }
    cout << endl;
}

void using_std_strings() {
    string first = "Edu ";
    string last = "Finn";

    string pFullName = first + last;
    cout << "combined: " << pFullName << endl;
    printf("%s", pFullName.c_str());
    cout << endl;
}

void using_string_streams() {
    cout << endl;
    int a{5}, b{6};
    int sum = a + b;
    cout << "Sum of " << a << " & " << b << " is : " << sum << endl;
    // string output = "Sum of " + a + " & " + b + " is : " + sum; // this doesn't work
    stringstream ss; // can do both read and write
    istringstream is; // can only read from this
    ostringstream os; // can only write to this
    ss << "Sum of " << a << " & " << b << " is : " << sum << endl;
    cout << ss.str();
    ss.str("empty");
    cout << ss.str() << endl;
    ss << sum; // appends to previous buffer
    cout << ss.str() << endl;
    ss.str("");
    ss << sum;
    cout << ss.str() << endl;
    auto ssum = to_string(sum);
    cout << ssum << endl;
    cout << endl;

    string data = "12 98 21";
    ss.str(data);
    while(ss >> a) {
        cout << a << endl;
    }
    int x = stoi("56");
    cout << x << endl;
    cout << endl;
}

int main() {
    raw_strings();
    std_string();
    using_std_strings();
    using_string_streams();
    return 0;
}
