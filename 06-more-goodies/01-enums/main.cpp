#include <iostream>

using namespace std;

#define RED_I 0
const int GREEN_I = 1;

void fillColorInt(int color) {
    if (color == 0) {
        cout << "RED " << endl;
    } else if (color == 1) {
        cout << "GREEN" << endl;
    } else {
        cout << "Unknown color" << endl;
    }
}

enum class Color : long {
    RED = 5, GREEN, BLUE
};

void fillColor(Color color) {
    if (color == Color::RED) {
        cout << "RED " << endl;
    } else if (color == Color::GREEN) {
        cout << "GREEN" << endl;
    } else if (color == Color::BLUE) {
        cout << "BLUE" << endl;
    }
}

enum TrafficLight : char {
    RED = 'r', YELLOW = 'y', GREEN = 'g'
};

int main() {
    int c = RED_I;
    fillColorInt(c);
    fillColorInt(GREEN_I);
    fillColorInt(5);
    cout << endl;

    Color color = Color::RED;
    fillColor(color);
    fillColor(Color::GREEN);
    fillColor(static_cast<Color>(2));
    // fillColor(5); // this will not compile
    cout << endl;

    long x = static_cast<long>(Color::BLUE);
    char y = RED; // TrafficLight enum
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;

    cout << endl;
    return 0;
}
