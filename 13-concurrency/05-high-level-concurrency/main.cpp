#include <future>
#include <iostream>
#include <thread>
using std::cout, std::endl;

void downloader() {
    using namespace std::chrono_literals;
    for (int i = 0; i < 10; ++i) {
        cout << ".";
        std::this_thread::sleep_for(100ms);
    }
    cout << endl;
}

void use_downloader() {
    std::future<void> result = std::async(downloader);
    cout << "main1 thread continues execution..." << endl;
    result.get();
}

int operation(int count) {
    using namespace std::chrono_literals;
    int sum{};
    for (int i = 0; i < count; ++i) {
        sum += i;
        cout << ".";
        std::this_thread::sleep_for(100ms);
    }
    cout << endl;
    return sum;
}
void check_load_policy() {
    using namespace std::chrono_literals;
    std::future<int> result = std::async(std::launch::async, operation, 10);
    //std::future<int> result = std::async(std::launch::deferred, operation, 10);
    std::this_thread::sleep_for(1s);
    cout << "main2 thread continues execution..." << endl;
    if (result.valid()) {
        // result.wait(); // this block current thread for infinity
        auto timepoint = std::chrono::system_clock::now() + 1s;
        // auto status = result.wait_for(1s);
        auto status = result.wait_until(timepoint);
        switch (status) {
            case std::future_status::deferred:
                cout << "Task is synchronous" << endl;
                break;
            case std::future_status::ready:
                cout << "Result is ready" << endl;
                break;
            case std::future_status::timeout:
                cout << "Task is still running" << endl;
                break;
        }
        auto sum = result.get();
        cout << "result: " << sum << endl;
    }
}

int operation1(std::promise<int> &data) {
    using namespace std::chrono_literals;
    auto f = data.get_future();
    cout << "[task] waiting for count" << endl;
    auto count = f.get();
    cout << "[task] count acquired" << endl;
    int sum{};
    for (int i = 0; i < count; ++i) {
        sum += i;
        cout << ".";
        std::this_thread::sleep_for(100ms);
    }
    cout << endl;
    return sum;
}
void use_promise() {
    using namespace std::chrono_literals;
    std::promise<int> data;
    std::future<int> result = std::async(std::launch::async, operation1, std::ref(data));
    std::this_thread::sleep_for(1s);
    cout << "main3 setting data in promise..." << endl;
    data.set_value(10);
    if (result.valid()) {
        auto sum = result.get();
        cout << "Sum with promise: " << sum << endl;
    }
}

int operation2(std::promise<int> &data) {
    using namespace std::chrono_literals;
    auto f = data.get_future();
    try {
        cout << "[task1] waiting for count" << endl;
        auto count = f.get();
        cout << "[task1] count acquired" << endl;
        int sum{};
        for (int i = 0; i < count; ++i) {
            sum += i;
            cout << ".";
            std::this_thread::sleep_for(100ms);
        }
        cout << endl;
        return sum;
    } catch (std::exception &ex) {
        cout << "[task1] exception: " << ex.what() << endl;
    }
    return -1;
}
void exception_handling() {
    using namespace std::chrono_literals;
    std::promise<int> data;
    std::future<int> result = std::async(std::launch::async, operation2, std::ref(data));
    std::this_thread::sleep_for(1s);
    cout << "main4 setting data in promise..." << endl;
    try {
        throw std::runtime_error("Data not available");
    } catch (std::exception &ex) {
        data.set_exception(std::make_exception_ptr(ex));
    }
}

int main() {
    use_downloader();
    cout << endl;

    check_load_policy();
    cout << endl;

    use_promise();
    cout << endl;

    exception_handling();
    cout << endl;
    return 0;
}
