#include <iostream>
#include <list>
using std::cout, std::endl;

std::list<int> data;
const int SIZE = 5000000;
void download() {
    cout << "[Downloader] started download..." << endl;
    for (int i = 0; i < SIZE; ++i) {
        data.emplace_back(i);
    }
    cout << "[Downloader] finished download" << endl;
}

int main() {
    cout << "[main] user started an operation" << endl;
    download();
    cout << "[main] user started another operation" << endl;
    cout << endl;
    return 0;
}
