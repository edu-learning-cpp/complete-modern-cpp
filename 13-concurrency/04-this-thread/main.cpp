#include <iostream>
#include <thread>
#include <chrono>
#include <pthread.h>
using std::cout, std::endl;
using namespace std::chrono_literals;

void my_process() {
    std::string name = "My Thread";
    pthread_setname_np(name.c_str()); // Mac OS only

    cout << "thread_id: " << std::this_thread::get_id() << endl;

    for (int i = 0; i < 10; ++i) {
        std::this_thread::sleep_for(1s);
        cout << i << " ";
    }
    cout << endl;
}

int main() {
    std::thread t1(my_process);
    auto id = t1.get_id();
    cout << "process thread id: " << id << endl;

    auto handle = t1.native_handle();
    char name[16];
    std::this_thread::sleep_for(100ms);
    pthread_getname_np(handle, &name[0], sizeof(name)); // Mac OS only
    cout << name << endl;

    auto cores = std::thread::hardware_concurrency();
    cout << "Cores: " << cores << endl;

    t1.join();
    cout << endl;
    return 0;
}
