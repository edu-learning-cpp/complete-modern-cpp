#include <iostream>
#include <list>
#include <thread>
#include <mutex>

using std::cout, std::endl;

std::list<int> data;
const int SIZE = 5000000;
class String {
public:
    String() {
        cout << "String()" << endl;
    }
    String(const String &) {
        cout << "String(const String&)" << endl;
    }
    String & operator=(const String &) {
        cout << "operator=(const String&)" << endl;
        return *this;
    }
    ~String() {
        cout << "~String()" << endl;
    }
};

void download(const String &filename) {
    //cout << "[Downloader] started download of file " << filename << endl;
    for (int i = 0; i < SIZE; ++i) {
        data.emplace_back(i);
    }
    cout << "[Downloader] finished download" << endl;
}

std::list<int> data1;
const int SIZE1 = 10000;
std::mutex myMutex;
void download1() {
    for (int i = 0; i < SIZE1; ++i) {
        myMutex.lock();
        data1.emplace_back(i);
        myMutex.unlock();
    }
}
void download2() {
    for (int i = 0; i < SIZE1; ++i) {
        myMutex.lock();
        data1.emplace_back(i);
        myMutex.unlock();
    }
}

void use_mutex() {
    std::thread thDownloader1(download1);
    std::thread thDownloader2(download2);
    thDownloader1.join();
    thDownloader2.join();
    cout << "data1 size: " << data1.size() << endl;
}

std::list<int> data2;
const int SIZE2 = 10000;
std::mutex myMutex2;
void download3() {
    for (int i = 0; i < SIZE2; ++i) {
        std::lock_guard<std::mutex> mtx(myMutex2);
        data2.emplace_back(i);
        if (i == 500) {
            return;
        }
    }
}
void download4() {
    for (int i = 0; i < SIZE2; ++i) {
        std::lock_guard<std::mutex> mtx(myMutex2);
        data2.emplace_back(i);
    }
}

void use_lock_guard() {
    std::thread thDownloader1(download3);
    std::thread thDownloader2(download4);
    thDownloader1.join();
    thDownloader2.join();
    cout << "data2 size: " << data2.size() << endl;
}

int main() {
    String filename;
    cout << "[main] user started an operation" << endl;
    std::thread thDownloader(download, std::cref(filename));
    cout << "[main] user started another operation" << endl;
    if (thDownloader.joinable()) {
        thDownloader.join();
    }
    cout << endl;

    use_mutex();
    cout << endl;

    use_lock_guard();
    cout << endl;
    return 0;
}
