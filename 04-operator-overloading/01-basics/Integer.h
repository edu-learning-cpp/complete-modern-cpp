//
// Created by Eduard Luhtonen on 02.08.20.
//

#ifndef INC_01_BASICS_INTEGER_H
#define INC_01_BASICS_INTEGER_H

class Printer {};

class Integer {
    int *m_pInt;
public:
    Integer();
    Integer(int value);
    Integer(const Integer &obj);
    Integer(Integer &&obj);
    int geValue() const;
    void setValue(int value);
    ~Integer();
    // Integer operator+(const Integer &a) const;
    Integer & operator++();
    Integer operator++(int);
    bool operator==(const Integer &obj) const;
    Integer & operator=(const Integer &a);
    Integer & operator=(Integer &&a);

    void operator()();
    friend std::istream& operator>>(std::istream& input, Integer &a);
    friend Printer;
};

#endif //INC_01_BASICS_INTEGER_H
