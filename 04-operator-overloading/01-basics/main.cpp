#include <iostream>
#include "Integer.h"
#include <memory>

using std::cout, std::endl;

void print(Integer &v) {
    cout << v.geValue() << endl;
}

Integer operator+(const Integer &a, const Integer &b) {
    return Integer(a.geValue() + b.geValue());
}

void use_integer() {
    Integer a(2), b(3);
    Integer sum = a + b;
    cout << "sum: ";
    print(sum);
    cout << "++sum: ";
    print(++sum);
    cout << "sum++: ";
    cout << (sum++).geValue() << endl;
    cout << "sum after: ";
    print(sum);

    if (a == b) {
        cout << "same" << endl;
    } else {
        cout << "Not same" << endl;
    }
    cout << endl;

    Integer c;
    c = a;
    cout << "c: ";
    print(c);
    a = a;
    cout << "a: ";
    print(a);
    cout << endl;
}

Integer operator+(int x, Integer y) {
    Integer temp;
    temp.setValue(x + y.geValue());
    return temp;
}

std::ostream& operator<<(std::ostream &os, const Integer& i) {
    os << i.geValue();
    return os;
}

std::istream& operator>>(std::istream& input, Integer &a) {
    int x;
    input >> x;
    *a.m_pInt = x;
    return input;
}

void global_overloads() {
    Integer a(1), b(3);
    Integer sum = a + 5;
    cout << "sum: " << sum.geValue() << endl;
    sum = 5 + b;
    cout << "sum: " << sum << endl;

    b();
    cout << endl;
}

void CreateInteger() {
    Integer *p = new Integer;
    p->setValue(5);
    cout << p->geValue() << endl;
    delete p;
}

class IntPtr {
    Integer *m_p;
public:
    IntPtr(Integer *p) : m_p(p) {}
    ~IntPtr() {
        delete m_p;
    }
    Integer *operator->() {
        return m_p;
    }
    Integer &operator*() {
        return *m_p;
    }
};

void CreateInt() {
    IntPtr p = new Integer;
    p->setValue(6);
    cout << (*p).geValue() << endl;
}

void process(std::unique_ptr<Integer> ptr) {
    cout << ptr->geValue() << endl;
}

void process(const std::shared_ptr<Integer>& ptr) {
    cout << ptr->geValue() << endl;
}

void CreateUniqueInteger() {
    std::unique_ptr<Integer> p(new Integer);
    p->setValue(5);
    process(std::move(p));
    // cannot use the pointer after move
}

void CreateSharedInteger() {
    std::shared_ptr<Integer> p(new Integer);
    p->setValue(6);
    process(p);
    // pointer is copied, so it can be used after this line
}

void smart_pointers() {
    cout << endl;
    CreateInteger();
    cout << endl;
    CreateInt();
    cout << endl;

    CreateUniqueInteger();
    cout << endl;
    CreateSharedInteger();
    cout << endl;
}

int main() {
    use_integer();
    global_overloads();
    smart_pointers();
    return 0;
}
