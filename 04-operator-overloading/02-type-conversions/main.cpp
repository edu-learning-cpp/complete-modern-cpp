#include <iostream>
#include <utility>
#include "Integer.h"

using namespace std;

void basics() {
    int a = 5, b = 2;
    float f = a / b;
    cout << f << endl;
    f = (float) a / b; // c style conversion
    cout << f << endl;
    f = static_cast<float>(a) / b; // c++ style conversion
    cout << f << endl;

    // danger of using c style cast
    char *p = (char *)&a; // this compiles, but will cause an error at runtime
    // char *p1 = static_cast<char*>(&a); // this will not compile
    char *p2 = reinterpret_cast<char*>(&a); // forcing cast
    cout << endl;
}

void print(const Integer &a) {
    cout << a.geValue() << endl;
}

void from_primitive_to_user_types() {
    cout << endl;
    Integer a1{5};
    cout << a1.geValue() << endl;
    Integer a2 = 5;
    cout << a2.geValue() << endl;
    //Integer a_str = "abc"; // will not compile

    print(5);
    a1 = 6;
    print(a1);

    cout << endl;
}

void from_user_to_primitive_types() {
    cout << endl;
    Integer a1{5};
    int x = static_cast<int>(a1);
    cout << x << endl;
}

class Product {
    Integer m_Id;
public:
    Product(const Integer &mId) {
        cout << "Product(const Integer &)" << endl;
        m_Id = mId;
    }
    ~Product() {
        cout << "~Product()" << endl;
    }
};

class BetterProduct {
    Integer m_Id;
public:
    explicit BetterProduct(const Integer mId): m_Id(mId) {
        cout << "BetterProduct(const Integer &)" << endl;
    }
    ~BetterProduct() {
        cout << "~BetterProduct()" << endl;
    }
};

void initialisation_vs_assignment() {
    cout << endl;
    // Initialisation
    Integer a1(5);
    // Assignment
    Integer a2;
    a2 = 6;
}

void use_product() {
    cout << endl;
    Product p(5);
}

void use_better_product() {
    cout << endl;
    BetterProduct p(5);
}

int main() {
    basics();
    from_primitive_to_user_types();
    from_user_to_primitive_types();
    initialisation_vs_assignment();
    use_product();
    use_better_product();
    return 0;
}
