//
// Created by Eduard Luhtonen on 10.08.20.
//

#ifndef INC_02_TYPE_CONVERSIONS_INTEGER_H
#define INC_02_TYPE_CONVERSIONS_INTEGER_H


class Integer {
    int *m_pInt;
public:
    Integer();
    Integer(int value);
    Integer(const Integer &obj);
    Integer(Integer &&obj);
    int geValue() const;
    void setValue(int value);
    ~Integer();
    // Integer operator+(const Integer &a) const;
    Integer & operator++();
    Integer operator++(int);
    bool operator==(const Integer &obj) const;
    Integer & operator=(const Integer &a);
    Integer & operator=(Integer &&a);

    void operator()();
    explicit operator int(); // type conversion operator
};


#endif //INC_02_TYPE_CONVERSIONS_INTEGER_H
