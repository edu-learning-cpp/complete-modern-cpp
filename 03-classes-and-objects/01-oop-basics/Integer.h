//
// Created by Eduard Luhtonen on 02.08.20.
//

#ifndef INC_01_OOP_BASICS_INTEGER_H
#define INC_01_OOP_BASICS_INTEGER_H


class Integer {
    int *m_pInt;
public:
    Integer();
    explicit Integer(int value);
    Integer(const Integer &obj);
    Integer(Integer &&obj);
    ~Integer();
    int GetValue() const;
    void SetValue(int value);
};


#endif //INC_01_OOP_BASICS_INTEGER_H
