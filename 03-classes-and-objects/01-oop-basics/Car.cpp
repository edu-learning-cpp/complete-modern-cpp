//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "Car.h"

using std::cout, std::endl;

int Car::totalCount = 0;

Car::Car() : Car(0) {
    cout << "Car()" << endl;
}

Car::Car(float amount) : Car(amount, 0) {
    cout << "Car(float)" << endl;
}

Car::Car(float amount, int pass) {
    cout << "Car(float, int)" << endl;
    ++totalCount;
    fuel = amount;
    passengers = pass;
}

Car::~Car() {
    --totalCount;
    cout << "~Car()" << endl;
}

void Car::fillFuel(float amount) {
    fuel = amount;
}

void Car::accelerate() {
    this->speed++;
    this->fuel -= 0.5f;
}

void Car::brake() {
    speed = 0;
}

void Car::addPassengers(int passengers) {
    this->passengers = passengers;
}

void Car::dashboard() const {
    cout << "Fuel: " << fuel << endl;
    cout << "Speed: " << speed << endl;
    cout << "Passengers: " << passengers << endl;
}

void Car::showCount() {
    cout << "Total cars: " << totalCount << endl;
}
