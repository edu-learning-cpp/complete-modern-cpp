//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "structures.h"

using std::cout, std::endl;

struct Point {
    int x;
    int y;
};
void drawLine(int x1, int y1, int x2, int y2) {
    cout << "start: " << x1 << ", " << y1 << endl;
    cout << "end: " << x2 << ", " << y2 << endl;
}
void drawLine(Point start, Point end) {
    cout << "start: " << start.x << ", " << start.y << endl;
    cout << "end: " << end.x << ", " << end.y << endl;
}

void structures() {
    drawLine(1, 2, 3, 4);
    drawLine(Point{1, 2}, Point{3, 4});
    cout << endl;
}
