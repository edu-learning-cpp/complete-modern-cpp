#include <iostream>
#include "Car.h"
#include "structures.h"
#include "Integer.h"
#include "Integer2.h"

using std::cout, std::endl;

void use_car() {
    Car car;
    car.dashboard();
    car.fillFuel(6);
    car.accelerate();
    car.accelerate();
    car.accelerate();
    car.accelerate();
    car.dashboard();
}

void use_car2() {
    Car car(4);
    car.dashboard();
    car.addPassengers(2);
    car.accelerate();
    car.accelerate();
    car.accelerate();
    car.accelerate();
    car.dashboard();
}

void use_car3() {
    Car::showCount();
    const Car car;
    // with const object functions which changes state of an object are not allowed
//    car.fillFuel(6);
//    car.accelerate();
//    car.accelerate();
//    car.accelerate();
//    car.accelerate();
    // this function doesn't change state of an object
    car.dashboard();

    // static class members
    Car::showCount();
    Car c1, c2;
    Car::showCount();
}

void print_pointer(int *p) {
    cout << p << endl;
    cout << *p << endl;
}

void use_integer() {
    int *p1 = new int(5);
    print_pointer(p1);
    // shallow copy
    int *p2 = p1; // copy address
    print_pointer(p2);
    // deep copy
    int *p3 = new int(*p1); // create new pointer with new address but same value
    print_pointer(p3);
    cout << endl;

    Integer i(5);
    cout << i.GetValue() << endl;
    Integer i2(i);
    cout << i2.GetValue() << endl;
    cout << endl;
}

void use_integer2() {
    Integer2 i1;
    cout << i1.getValue() << endl;
    Integer2 i2(3);
    cout << i2.getValue() << endl;
//    Integer2 i3(i2); // delete removed copy constructor

    i1.setValue(5);
    cout << i1.getValue() << endl;
    // i1.setValue(67.1f); // without delete this would be possible
    cout << endl;
}

// returns r-value
int add(int x, int y) {
    return x + y;
}

// return l-value
int & transform(int &x) {
    x *= x;
    return x;
}

void print(int &x) {
    cout << "print(int&)" << endl;
}
void print(const int &x) {
    cout << "print(const int&)" << endl;
}
void print(int &&x) {
    cout << "print(int&&)" << endl;
}

void use_r_l_values() {
    // x, y & z are l-values, 5, 10 & 8 are r-values
    int x = 5;
    int y = 10;
    int z = 8;

    // expression returns r-value
    int result = (x + y) * z;
    cout << result << endl;

    // expression returns l-value
    ++x = 6;
    cout << x << endl;

    cout << add(x, y) << endl;
    cout << transform(x) << endl;
    cout << "x after transform: " << x << endl;

    int &&r1 = 10;
    int &&r2 = add(3, 2);

    // int &&r3 = x;    // cannot do this
    int &ref = transform(x);
    cout << ref << endl;

    const int &ref2 = 3;
    cout << endl;

    print(x);
    print(3);
    cout << endl;
}

Integer add(const Integer &a, const Integer &b) {
    Integer temp;
    temp.SetValue(a.GetValue() + b.GetValue());
    cout << "temp address: " << &temp << endl;
    return temp;
}

void print(Integer &i) {
    cout << i.GetValue() << endl;
}

void use_integer3() {
    Integer a(1), b(3);
    print(a);
    print(b);
    a.SetValue(add(a, b).GetValue());
    print(a);

    Integer c = add(a, b);
    cout << "c address: " << &c << endl;
    print(c);
}

void process(Integer val) {
    print(val);
}

void use_integer4() {
    Integer a(3);
    auto b(a);
    print(b);
    process(a);
    auto c(std::move(a));
    print(c);
    process(std::move(c));
}

int main() {
    use_car();
    use_car2();
    structures();
    use_car3();
    use_integer();
    use_integer2();
    use_r_l_values();
    use_integer3();
    cout << endl;
    use_integer4();
    return 0;
}
