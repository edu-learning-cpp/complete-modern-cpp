//
// Created by Eduard Luhtonen on 02.08.20.
//

#ifndef INC_01_OOP_BASICS_CAR_H
#define INC_01_OOP_BASICS_CAR_H


class Car {
private:
    float fuel{0};
    float speed{0};
    int passengers{0};
    static int totalCount;
public:
    Car();
    explicit Car(float amount);
    Car(float amount, int pass);
    ~Car();
    void fillFuel(float amount);
    void accelerate();
    void brake();
    void addPassengers(int count);
    void dashboard() const;
    static void showCount();
};


#endif //INC_01_OOP_BASICS_CAR_H
