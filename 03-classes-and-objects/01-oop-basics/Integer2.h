//
// Created by Eduard Luhtonen on 02.08.20.
//

#ifndef INC_01_OOP_BASICS_INTEGER2_H
#define INC_01_OOP_BASICS_INTEGER2_H


class Integer2 {
    int m_value = 0;
public:
    Integer2() = default;
    Integer2(int value) {
        m_value = value;
    }
    Integer2(const Integer2 &) = delete; // by default compiler generates copy constructor
    int getValue() const {
        return m_value;
    }
    void setValue(int value) {
        m_value = value;
    }
    void setValue(float value) = delete;
};

#endif //INC_01_OOP_BASICS_INTEGER2_H
