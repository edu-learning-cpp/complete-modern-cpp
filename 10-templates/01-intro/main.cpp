#include <iostream>
#include <typeinfo>

using namespace std;

//int myMax(int x, int y) {
//    return x > y ? x : y;
//}
//
//float myMax(float x, float y) {
//    return x > y ? x : y;
//}

template<typename T>
T myMax(T x, T y) {
    cout << typeid(T).name() << endl;
    return x > y ? x : y;
}

template<>
const char *myMax(const char *x, const char *y) {
    cout << "myMax<const char*>()" << endl;
    return strcmp(x, y) > 0 ? x : y;
}

void explicit_specialisation() {
    const char *b{"B"};
    const char *a{"A"};
    auto s = myMax(a, b);
    cout << s << endl;
}

template<int size>
void print() {
    cout << size << endl;
}

template<typename T>
T mySum(T *pArr, int size) {
    T sum{};
    for (int i = 0; i < size; ++i) {
        sum += pArr[i];
    }
    return sum;
}

template<typename T, int size>
T mySum2(T (&pArr)[size]) {
    T sum{};
    for (int i = 0; i < size; ++i) {
        sum += pArr[i];
    }
    return sum;
}

void non_type_template_arguments() {
    print<5>();

    int arr[]{3, 1, 9, 7};
    int sum = mySum(arr, 4);
    cout << sum << endl;

    int (&ref)[4] = arr;
    sum = mySum2(arr);
    cout << sum << endl;
}

int main() {
    auto num = myMax(3, 8);
    cout << num << endl;
    auto num2 = myMax(3.2, 5.8);
    cout << num2 << endl;
    cout << endl;

    cout << myMax<float>(3, 6.5) << endl;
    int (*pfn)(int, int) = myMax;
    cout << pfn(3, 5) << endl;
    cout << endl;

    explicit_specialisation();
    cout << endl;

    non_type_template_arguments();
    cout << endl;

    return 0;
}
