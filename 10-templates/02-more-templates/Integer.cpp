//
// Created by Eduard Luhtonen on 21/08/2020.
//

#include "Integer.h"
#include <iostream>

Integer::Integer() {
    std::cout << "Integer()" << std::endl;
    m_pInt = new int(0);
}

Integer::Integer(int value) {
    std::cout << "Integer(int)" << std::endl;
    m_pInt = new int(value);
}

Integer::Integer(const Integer &obj) {
    std::cout << "Integer(const Integer &)" << std::endl;
    m_pInt = new int(*obj.m_pInt);
}

Integer::Integer(Integer &&obj) {
    std::cout << "Integer(Integer &&)" << std::endl;
    m_pInt = obj.m_pInt;
    obj.m_pInt = nullptr;
}

Integer::~Integer() {
    std::cout << "~Integer()" << std::endl;
    delete m_pInt;
}

int Integer::getValue() const {
    return *m_pInt;
}

void Integer::setValue(int value) {
    *m_pInt = value;
}
