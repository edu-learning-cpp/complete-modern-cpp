#include "Integer.h"
#include <iostream>

using namespace std;

class Employee {
    string name;
    Integer id;
public:
    Employee(const string &name, const Integer &id) : name(name), id(id) {
        cout << "Employee(const string&, const Integer&)" << endl;
    }

    Employee(string &&name, Integer &&id) : name(name), id(move(id)) {
        cout << "Employee(string&&, Integer&&)" << endl;
    }

    template<typename T1, typename T2>
    Employee(T1 &&name, T2 &&id):
            name(std::forward<T1>(name)),
            id(std::forward<T2>(id)) {
        cout << "Employee(T1&&, T2&&)" << endl;
    }
};

template<typename T1, typename T2>
Employee *create(T1 &&a, T2 &&b) {
    return new Employee(std::forward<T1>(a), std::forward<T2>(b));
}

void using_forward() {
    Employee emp1{"Edu", 1001};
    string name = "Edu";
    Employee emp2{name, 1002};
    Integer val{1003};
    Employee emp4{"Edu", val};

    create("Edu", Integer(1004));
}

std::ostream& operator<<(std::ostream &os, const Integer& i) {
    os << i.getValue();
    return os;
}

template<typename T>
void print(std::initializer_list<T> args) {
    cout << "print with initializer list" << endl;
    for (const auto &x : args) {
        cout << x << " ";
    }
    cout << endl;
}

void print() {
    cout << endl;
}

// template parameter pack
template<typename T, typename... Params>
void print(T &&a, Params &&... args) {
    cout << a;
    if (sizeof...(args) > 0) {
        cout << " | ";
    }
    print(std::forward<Params>(args)...);
}

void variadic_templates() {
    print({1, 2, 3, 4});
    print(1, 2.3, "4", 5.6f);
    Integer val{2};
    print(1, val, Integer{3});
}

int main() {
    using_forward();
    cout << endl;

    variadic_templates();

    return 0;
}
