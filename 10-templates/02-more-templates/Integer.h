//
// Created by Eduard Luhtonen on 21/08/2020.
//

#ifndef INC_02_MORE_TEMPLATES_INTEGER_H
#define INC_02_MORE_TEMPLATES_INTEGER_H


class Integer {
    int *m_pInt;
public:
    // default constructor
    Integer();

    // parametrised constructor
    Integer(int value);

    // Copy constructor
    Integer(const Integer &obj);

    // Move constructor
    Integer(Integer &&obj);

    ~Integer();

    int getValue() const;
    void setValue(int value);
};


#endif //INC_02_MORE_TEMPLATES_INTEGER_H
