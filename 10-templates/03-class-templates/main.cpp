#include <iostream>
#include <vector>
#include <list>
#include <type_traits>

using std::cout, std::endl;

class IntStack {
    int buffer[512];
    int m_top{-1};
public:
    void push(int elem) {
        buffer[++m_top] = elem;
    }

    void pop() {
        --m_top;
    }

    int top() {
        return buffer[m_top];
    }

    bool isEmpty() const {
        return m_top == -1;
    }
};

template<typename T, int size>
class Stack {
    T buffer[size];
    int m_top{-1};
public:
    Stack() = default;

    Stack(const Stack &obj) {
        m_top = obj.m_top;
        for (int i = 0; i <= m_top; ++i) {
            buffer[i] = obj.buffer[i];
        }
    }

    void push(const T &elem) {
        buffer[++m_top] = elem;
    }

    void pop();

    const T &top() const {
        return buffer[m_top];
    }

    bool isEmpty() const {
        return m_top == -1;
    }

    static Stack create();
};

template<typename T, int size>
void Stack<T, size>::pop() {
    --m_top;
}

template<typename T, int size>
Stack<T, size> Stack<T, size>::create() {
    return Stack<T, size>();
}

void class_templates() {
    IntStack si;
    si.push(3);
    si.push(1);
    si.push(6);
    si.push(9);
    while (!si.isEmpty()) {
        cout << si.top() << " ";
        si.pop();
    }
    cout << endl;

    Stack<float, 10> s;
    s.push(3);
    s.push(1);
    s.push(6);
    s.push(9);
    while (!s.isEmpty()) {
        cout << s.top() << " ";
        s.pop();
    }
    cout << endl;

    Stack<float, 10> s2 = Stack<float, 10>::create();
    s2.push(2);
    s2.push(4);
    s2.push(5);
    s2.push(8);

    auto s3(s2);
    while (!s3.isEmpty()) {
        cout << s3.top() << " ";
        s3.pop();
    }
    cout << endl;
}

template<typename T>
class PrettyPrinter {
    T *m_data;
public:
    PrettyPrinter(T *data) : m_data(data) {}

    void print() {
        cout << "{" << *m_data << "}" << endl;
    }

    T *getData() {
        return m_data;
    }
};

template<>
class PrettyPrinter<char *> {
    char *m_data;
public:
    PrettyPrinter(char *data) : m_data(data) {}

    void print() {
        cout << "{" << m_data << "}" << endl;
    }

    char *getData() {
        return m_data;
    }
};

template<>
void PrettyPrinter<std::vector<int>>::print() {
    cout << "{ ";
    for (const auto &x : *m_data) {
        cout << x << " ";
    }
    cout << "}" << endl;
}

void template_specialisation() {
    int data = 5;
    float f = 8.2f;
    PrettyPrinter<int> p1(&data);
    p1.print();
    PrettyPrinter<float> p2(&f);
    p2.print();

    char *p{"Hello World"};
    PrettyPrinter<char *> p3(p);
    p3.print();
    char *pData = p3.getData();
    cout << endl;

    std::vector<int> v{1, 2, 3, 4, 5};
    PrettyPrinter<std::vector<int>> pv(&v);
    pv.print();
}

template<typename T, int columns>
class PrettierPrinter {
    T *m_data;
public:
    PrettierPrinter(T *data) : m_data(data) {}
    void print() {
        cout << "Columns: " << columns << endl;
        cout << "{" << *m_data << "}" << endl;
    }
    T *getData() {
        return m_data;
    }
};

template<typename T>
class PrettierPrinter<T, 80> {
    T *m_data;
public:
    PrettierPrinter(T *data) : m_data(data) {}
    void print() {
        cout << "Using 80 Columns" << endl;
        cout << "{" << *m_data << "}" << endl;
    }
    T *getData() {
        return m_data;
    }
};

template<typename T>
class SmartPointer {
    T *m_ptr;
public:
    SmartPointer(T *ptr) : m_ptr(ptr) {}
    T *operator->() {
        return m_ptr;
    }
    T &operator*() {
        return *m_ptr;
    }
    ~SmartPointer() {
        delete m_ptr;
    }
};

template<typename T>
class SmartPointer<T[]> {
    T *m_ptr;
public:
    SmartPointer(T *ptr) : m_ptr(ptr) {}
    T &operator[](int index) {
        return m_ptr[index];
    }
    ~SmartPointer() {
        delete[] m_ptr;
    }
};

void partial_specialisation() {
    int data = 500;
    PrettierPrinter<int, 40> p1(&data);
    p1.print();
    PrettierPrinter<int, 80> p2(&data);
    p2.print();
    cout << endl;

    SmartPointer<int> s1{new int(3)};
    cout << *s1 << endl;
    SmartPointer<int[]> s2{new int[5]};
    s2[0] = 5;
    cout << s2[0] << endl;
}

const char* GetErrorMessage(int errorNo) {
    return "Empty";
}

void ShowError(const char *(*pfn)(int)) {
}

typedef const char*(*PFN_T)(int);
void ShowErrorTypeDef(PFN_T pfn) {}

using PFN = const char *(*)(int);
void ShowErrorAlias(PFN pfn) {}

typedef std::vector<std::list<std::string>> NamesTypeDef;

template<typename T>
using Names = std::vector<std::list<T>>;

void type_aliases() {
    PFN_T pfn1 = GetErrorMessage;
    ShowError(pfn1);
    ShowErrorTypeDef(pfn1);

    PFN pfn2 = GetErrorMessage;
    ShowErrorAlias(pfn2);

    std::vector<std::list<std::string>> names1;
    NamesTypeDef names2;
    Names<std::string> names3;
}

template<typename T>
T myDivide(T a, T b) {
    static_assert(std::is_floating_point<T>::value == true, "Use floating point types only");
    if (std::is_floating_point<T>::value == false) {
        cout << "Use floating point types only" << endl;
        return 0;
    }
    return a / b;
}

template<typename T>
void check(T&&) {
    cout << std::boolalpha;
    cout << "Is reference? " << std::is_reference<T>::value << endl;

    cout << "After removing: "
    << std::is_reference<typename std::remove_reference<T>::type>::value << endl;
}

void type_traits() {
    cout << std::boolalpha
    << "Is integer? " << std::is_integral<int>::value << endl;

    //cout << myDivide(5, 2) << endl;
    cout << myDivide(5.6, 2.3) << endl;

    check(5);
    int val{5};
    check(val);
}

void using_static_assert() {
    if (sizeof(void *) != 4) {
        cout << "Not 32-bit mode" << endl;
    } else {
        cout << "Is 32-bit mode" << endl;
    }
    // static_assert(sizeof(void *) == 4, "Compile in 32-bit mode only"); // this will stop compilation
    static_assert(sizeof(void *) == 8, "Compile in 64-bit mode only");
}

int main() {
    class_templates();
    cout << endl;

    template_specialisation();
    cout << endl;

    partial_specialisation();
    cout << endl;

    type_aliases();
    cout << endl;

    type_traits();
    cout << endl;

    using_static_assert();
    cout << endl;

    return 0;
}
