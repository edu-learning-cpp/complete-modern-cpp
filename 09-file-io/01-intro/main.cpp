#include <iostream>
#include <filesystem>
using namespace std;
namespace fs = filesystem;

void using_raw_string() {
    string filename{"C:\\temp\\new.txt"};
    string filename1{R"(C:\temp\new.txt)"}; // raw string
    std::cout << filename << std::endl;
    std::cout << filename1 << std::endl;

    string message{R"MSG(C++ introduced filesystem API"(In C++17)")MSG"};
    cout << message << endl;
    cout << endl;
}

void using_filesystem() {
    string filename{R"(../)"};
    fs::path p{filename};
    for (auto& x : fs::directory_iterator(filename)) {
        cout << x << endl;
    }
}

int main() {
    using_raw_string();
    using_filesystem();
    return 0;
}
