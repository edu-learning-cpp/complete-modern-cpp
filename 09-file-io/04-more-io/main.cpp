#include <iostream>
#include <fstream>

using namespace std;

void write(string &filename) {
    ofstream out(filename);
    if (!out) {
        cout << "Could not open file for writing" << endl;
        return;
    }
    string message{"C++ was invented by Bjarne"};
    for (char ch : message) {
        out.put(ch);
    }

    out.close();
}

void read(string &filename) {
    ifstream in{filename};
    if (!in) {
        cout << "Source file not found" << endl;
        return;
    }
    char ch{};
    while (in.get(ch)) {
        cout << ch;
    }
    cout << endl;
    in.close();
}

void read_at_position(string &filename) {
    ifstream in{filename};
    if (!in) {
        cout << "Source file not found" << endl;
        return;
    }
    cout << "Current position is: " << in.tellg() << endl;
    in.seekg(5);
    cout << "Current position is: " << in.tellg() << endl;
    in.seekg(-6, ios::end);
    cout << "Current position is: " << in.tellg() << endl;
    char ch{};
    while (in.get(ch)) {
        cout << ch;
    }
    cout << endl;
    in.close();
}

void using_fstream() {
    string filename{"file.txt"};
    fstream stream{filename};
    if (!stream) {
        cout << "FILE DOES NOT EXISTS. Creating one..." << endl;
        ofstream out{filename};
        out.close();
        stream.open(filename);
    }
    stream << "Hello world" << endl;

    stream.seekg(0);
    string line;
    getline(stream, line);
    cout << line << endl;
}

struct Record {
    int id;
    char name[10];
};
void writeRecord(Record *p) {
    ofstream binstream{"records", ios::binary | ios::out};
    binstream.write((const char *) p, sizeof(Record));
}
Record getRecord() {
    ifstream input{"records", ios::binary | ios::in};
    Record r;
    input.read((char *) &r, sizeof(Record));
    return r;
}

void binary_io() {
    ofstream textstream{"data"};
    textstream << 12345678;
    textstream.close();

    ofstream binstream{"binary", ios::binary | ios::out};
    int num{12345678};
    binstream.write((const char *) &num, sizeof(num));
    binstream.close();

    num = 0;
    ifstream input{"binary", ios::binary | ios::in};
    input.read((char *) &num, sizeof(num));
    cout << num << endl;
    cout << endl;

    Record r{};
    r.id = 1001;
    strcpy(r.name, "Edu");
    writeRecord(&r);

    Record r2 = getRecord();
    cout << r2.id << ": " << r2.name << endl;
    cout << endl;
}

int main() {
    string filename{"test.txt"};
    write(filename);
    read(filename);
    cout << endl;

    read_at_position(filename);
    cout << endl;

    using_fstream();
    cout << endl;

    binary_io();

    return 0;
}
