cmake_minimum_required(VERSION 3.17)
project(04_more_io)

set(CMAKE_CXX_STANDARD 17)

add_executable(04_more_io main.cpp)