#include <iostream>
#include <fstream>
using namespace std;

void write(string &filename) {
    ofstream out{filename};
    out << "Hello World" << endl;
    out << 10 << endl;
    out.close();
}

void read(string &filename) {
    ifstream in/*{filename}*/;
    in.open(filename);
    string message;
    getline(in, message);
    int value{};
    in >> value;
    if (in.good()) {
        cout << "I/O operations are successful" << endl;
    } else {
        cout << "Some I/O operations failed" << endl;
    }
    in >> value;
    if (in.fail()) {
        cout << "could not read from file" << endl;
    }
    if (in.eof()) {
        cout << "end of file encountered" << endl;
    }
    in.close();

    cout << message << ":" << value << endl;
}

void read_with_error() {
    ifstream in{"dat.txt"};
//    if (!in.is_open()) {
//        cout << "Could not open the file" << endl;
//        return;
//    }
    if (in.fail()) {
        cout << "Could not open the file" << endl;
        return;
    }
    string message;
    getline(in, message);
    int value{};
    in >> value;
    in.close();

    cout << message << ":" << value << endl;
}

int main() {
    string filename{"data.txt"};
    write(filename);
    read(filename);
    cout << endl;

    read_with_error();
    return 0;
}
