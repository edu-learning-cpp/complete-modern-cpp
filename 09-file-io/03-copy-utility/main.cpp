#include <iostream>
#include <fstream>
#include <filesystem>
using namespace std;
namespace fs = filesystem;

int main() {
    path source(current_path());
    source /= "main.cpp";

    path dest(current_path());
    dest /= "copy.cpp";

    ifstream input{source};
    if (!input) {
        cout << "Source file not found" << endl;
        return -1;
    }
    ofstream output{dest};
    string line;
    while(!getline(input, line).eof()) {
        output << line << endl;
    }
    input.close();
    output.close();

    return 0;
}
